package androidstudio.beerexpresss;

/**
 * Created by JP on 11/23/2017.
 */

public class C_MenuClass {
    int itemimage;
    String itemname;
    String itemprice;
    String itemtype;
    Double itemcasequan;
    int itemquan;

    public C_MenuClass(){

    }

    public C_MenuClass(int itemimage, String itemname, String itemprice, String itemtype, int itemquan, Double itemcasequan) {
        this.itemimage = itemimage;
        this.itemname = itemname;
        this.itemprice = itemprice;
        this.itemtype = itemtype;
        this.itemquan = itemquan;
        this.itemcasequan = itemcasequan;
    }

    public int getItemimage() {
        return itemimage;
    }

    public void setItemimage(int itemimage) {
        this.itemimage = itemimage;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemprice() {
        return itemprice;
    }

    public void setItemprice(String itemprice) {
        this.itemprice = itemprice;
    }

    public int getItemquan() {
        return itemquan;
    }

    public void setItemquan(int itemquan) {
        this.itemquan = itemquan;
    }

    public String getItemtype() {
        return itemtype;
    }

    public void setItemtype(String itemtype) {
        this.itemtype = itemtype;
    }

    public Double getItemcasequan() {
        return itemcasequan;
    }

    public void setItemcasequan(Double itemcasequan) {
        this.itemcasequan = itemcasequan;
    }
}
