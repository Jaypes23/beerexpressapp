package androidstudio.beerexpresss;

/**
 * Created by JP on 4/29/2018.
 */

public class C_MessageClass {
    public String messagestatus;
    public String messageheader;
    public String messagecontent;

    public C_MessageClass(){
    }

    public C_MessageClass(String messagestatus, String messageheader, String messagecontent) {
        this.messagestatus = messagestatus;
        this.messageheader = messageheader;
        this.messagecontent = messagecontent;
    }

    public String getMessagestatus() {
        return messagestatus;
    }

    public void setMessagestatus(String messagestatus) {
        this.messagestatus = messagestatus;
    }

    public String getMessageheader() {
        return messageheader;
    }

    public void setMessageheader(String messageheader) {
        this.messageheader = messageheader;
    }

    public String getMessagecontent() {
        return messagecontent;
    }

    public void setMessagecontent(String messagecontent) {
        this.messagecontent = messagecontent;
    }
}
