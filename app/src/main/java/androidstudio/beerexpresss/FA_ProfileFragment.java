package androidstudio.beerexpresss;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 11/27/2017.
 */

public class FA_ProfileFragment extends android.support.v4.app.Fragment  {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //UI
    @BindView(R.id.header)
    TextView tvheader;
    @BindView(R.id.username)
    TextView tvname;
    @BindView(R.id.useremail)
    TextView tvemail;
    @BindView(R.id.useraddress)
    TextView tvaddress;
    @BindView(R.id.usercontact)
    TextView tvcontact;
    @BindView(R.id.btnUpdateProf)
    FancyButton btnUpdateProf;

    //Preference
    String USER_TYPE,name,email,address,contact;

    public FA_ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Bold.otf");
        Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");
        tvheader.setTypeface(headertextfont);
        tvname.setTypeface(regulartextfont);
        tvaddress.setTypeface(regulartextfont);
        tvemail.setTypeface(regulartextfont);
        tvcontact.setTypeface(regulartextfont);
    }


    public void InitializeFunctions(){
        RetrieveUserData();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        btnUpdateProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(),R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_updateprofile, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog matchDialog = dialogBuilder.create();
                Window window = matchDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
                final TextView tvheaderinfo = (TextView) dialogView.findViewById(R.id.headerinfo);
                final TextInputLayout tlname = (TextInputLayout) dialogView.findViewById(R.id.reg_tlname);
                final TextInputLayout tladdress = (TextInputLayout) dialogView.findViewById(R.id.reg_tladdress);
                final TextInputLayout tlcontact = (TextInputLayout) dialogView.findViewById(R.id.reg_tlcontact);
                final EditText etName = (EditText) dialogView.findViewById(R.id.reg_name);
                final EditText etAddress = (EditText) dialogView.findViewById(R.id.reg_address);
                final EditText etContact = (EditText) dialogView.findViewById(R.id.reg_contact);

                final FancyButton btnCancel = (FancyButton) dialogView.findViewById(R.id.buttonclose);
                final FancyButton btnUpdate = (FancyButton) dialogView.findViewById(R.id.buttonUpdate);

                final Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
                final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");

                tvheader.setTypeface(headerfont);
                tvheaderinfo.setTypeface(regulartextfont);
                tlname.setTypeface(regulartextfont);
                tladdress.setTypeface(regulartextfont);
                tlcontact.setTypeface(regulartextfont);

                etName.setText(name);
                etAddress.setText(address);
                etContact.setText(contact);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        matchDialog.dismiss();
                    }
                });

                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (TextUtils.isEmpty(etName.getText().toString())) {
                            etName.setError("Required");
                            return;
                        }
                        if (TextUtils.isEmpty(etAddress.getText().toString())) {
                            etAddress.setError("Required");
                            return;
                        }
                        if (TextUtils.isEmpty(etContact.getText().toString())) {
                            etContact.setError("Required");
                            return;
                        }
                        else{
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("name").setValue(etName.getText().toString());
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("address").setValue(etAddress.getText().toString());
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("contact").setValue(etContact.getText().toString());

                            SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                            editor.putString("username", etName.getText().toString());
                            editor.putString("useraddress", etAddress.getText().toString());
                            editor.putString("usercontact", etContact.getText().toString());
                            editor.commit();

                            tvname.setText("Your Name: "+etName.getText().toString());
                            tvaddress.setText("Your Address: "+etAddress.getText().toString());
                            tvcontact.setText("Your Contact #: "+etContact.getText().toString());

                            matchDialog.dismiss();

                            Toast.makeText(getActivity(), "Profile Update Successful!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                matchDialog.show();
            }
        });
    }



    public void RetrieveUserData(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");

        tvname.setText(name);
        tvemail.setText(email);
        tvaddress.setText(address);
        tvcontact.setText(contact);
    }


}
