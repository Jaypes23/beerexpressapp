package androidstudio.beerexpresss;

import java.util.ArrayList;

/**
 * Created by JP on 11/26/2017.
 */

public class C_OrderClass {
    public String ordernum;
    public String uid;
    public String address;
    public String contactnum;
    public String contactname;
    public String timestamp;
    public String changefor;
    public String carttotal;
    public String orderstatus;
    public String latitude;
    public String longitude;
    public String realaddress;
    public String landmark;
    public String additional;
    public String coolerrental;
    public String deliverytype;
    public String scheduledateandtime;
    public ArrayList<String> items;

    public String driverlat;
    public String driverlong;
    public String drivername;
    public String drivercontact;

    public C_OrderClass(){

    }

    public C_OrderClass(String ordernum, String uid, String address, String contactnum, String contactname,
                        String timestamp, String changefor, String carttotal, ArrayList<String> items, String latitude,
                        String longitude, String realaddress, String orderstatus,
                        String driverlat, String driverlong, String drivername, String drivercontact,
                        String landmark, String additional, String deliverytype, String scheduledateandtime, String coolerrental) {
        this.ordernum = ordernum;
        this.uid = uid;
        this.address = address;
        this.contactnum = contactnum;
        this.contactname = contactname;
        this.timestamp = timestamp;
        this.changefor = changefor;
        this.carttotal = carttotal;
        this.items = items;
        this.latitude = latitude;
        this.longitude = longitude;
        this.realaddress = realaddress;
        this.orderstatus = orderstatus;
        this.driverlat = driverlat;
        this.driverlong = driverlong;
        this.drivername = drivername;
        this.drivercontact = drivercontact;
        this.landmark = landmark;
        this.additional = additional;
        this.deliverytype = deliverytype;
        this.scheduledateandtime = scheduledateandtime;
        this.coolerrental = coolerrental;
    }

    public C_OrderClass(String scheduledateandtime, String orderstatus, String ordernum) {
        this.scheduledateandtime = scheduledateandtime;
        this.orderstatus = orderstatus;
        this.ordernum = ordernum;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactnum() {
        return contactnum;
    }

    public void setContactnum(String contactnum) {
        this.contactnum = contactnum;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getChangefor() {
        return changefor;
    }

    public void setChangefor(String changefor) {
        this.changefor = changefor;
    }

    public String getCarttotal() {
        return carttotal;
    }

    public void setCarttotal(String carttotal) {
        this.carttotal = carttotal;
    }

    public ArrayList<String> getItems() {
        return items;
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public String getOrdernum() {
        return ordernum;
    }

    public void setOrdernum(String ordernum) {
        this.ordernum = ordernum;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRealaddress() {
        return realaddress;
    }

    public void setRealaddress(String realaddress) {
        this.realaddress = realaddress;
    }

    public String getDriverlat() {
        return driverlat;
    }

    public void setDriverlat(String driverlat) {
        this.driverlat = driverlat;
    }

    public String getDriverlong() {
        return driverlong;
    }

    public void setDriverlong(String driverlong) {
        this.driverlong = driverlong;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getDrivercontact() {
        return drivercontact;
    }

    public void setDrivercontact(String drivercontact) {
        this.drivercontact = drivercontact;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getDeliverytype() {
        return deliverytype;
    }

    public void setDeliverytype(String deliverytype) {
        this.deliverytype = deliverytype;
    }

    public String getScheduledateandtime() {
        return scheduledateandtime;
    }

    public void setScheduledateandtime(String scheduledateandtime) {
        this.scheduledateandtime = scheduledateandtime;
    }

    public String getCoolerrental() {
        return coolerrental;
    }

    public void setCoolerrental(String coolerrental) {
        this.coolerrental = coolerrental;
    }
}
