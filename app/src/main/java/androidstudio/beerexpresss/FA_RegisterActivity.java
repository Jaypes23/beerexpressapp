package androidstudio.beerexpresss;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 9/22/2017.
 */

public class FA_RegisterActivity extends AppCompatActivity {

    //TextView
    @BindView(R.id.header)
    TextView tvheader;

    //TextInputLayout
    @BindView(R.id.reg_tlname)
    TextInputLayout tlname;
    @BindView(R.id.reg_tladdress)
    TextInputLayout tladdress;
    @BindView(R.id.reg_tlcontact)
    TextInputLayout tlcontact;
    @BindView(R.id.reg_tlbday)
    TextInputLayout tlBday;
    @BindView(R.id.login_usernametextinput)
    TextInputLayout tlemail;
    @BindView(R.id.login_passwordtextinput)
    TextInputLayout tlpassword;

    //EditText
    @BindView(R.id.reg_name)
    EditText etUserId;
    @BindView(R.id.reg_bday)
    EditText etBirthday;
    @BindView(R.id.reg_address)
    EditText etAddress;
    @BindView(R.id.reg_contact)
    EditText etContact;
    @BindView(R.id.reg_email)
    EditText etEmail;
    @BindView(R.id.reg_password)
    EditText etPassword;

    @BindView(R.id.ivcalendar)
    ImageView ivCalendar;

    //Button
    @BindView(R.id.btnBack)
    FancyButton btnBack;
    @BindView(R.id.btnRegister)
    FancyButton btnRegister;

    //Material Dialog
    MaterialDialog registerdialog;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Terms and Conditions
    @BindView(R.id.tvTermsIntro)
    TextView tvTermsIntro;
    @BindView(R.id.tvTerms)
    TextView tvTerms;
    @BindView(R.id.termsCheckbox)
    CheckBox cbterms;

    //Variables
    public int year,month,day,age;
    public String timestamp;
    public SimpleDateFormat currentTimeStamp;
    public String intentname, intentemail, intentuid;
    public String intentstatus;
    public String dateformatstatus = "false";
    public String USER_TYPE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        InitializeDesign();
        InitializeFunctions();
    }

    public void InitializeDesign(){
        Typeface headertextfont = Typeface.createFromAsset(getAssets(),  "fonts/Montserrat-Bold.otf");
        Typeface regulartextfont = Typeface.createFromAsset(getAssets(),  "fonts/Montserrat-Medium.otf");

        tvheader.setTypeface(headertextfont);

        tlname.setTypeface(regulartextfont);
        tladdress.setTypeface(regulartextfont);
        tlcontact.setTypeface(regulartextfont);
        tlBday.setTypeface(regulartextfont);
        tlemail.setTypeface(regulartextfont);
        tlpassword.setTypeface(regulartextfont);

        etUserId.setTypeface(regulartextfont);
        etAddress.setTypeface(regulartextfont);
        etContact.setTypeface(regulartextfont);
        etBirthday.setTypeface(regulartextfont);
        etEmail.setTypeface(regulartextfont);
        etPassword.setTypeface(regulartextfont);

        tvTerms.setTypeface(headertextfont);
        tvTermsIntro.setTypeface(regulartextfont);
    }

    public void InitializeFunctions() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        final Intent intent = getIntent();
        intentuid = intent.getStringExtra("user_id");
        intentname = intent.getStringExtra("user_name");
        intentemail = intent.getStringExtra("user_email");

        if(intentuid.equals("default")){
            intentstatus = "false";
        }else{
            intentstatus = "true";
        }

        if(intentstatus.equals("true")){
            etUserId.setText(intentname);
            etEmail.setText(intentemail);
            etUserId.setEnabled(false);
            etEmail.setEnabled(false);
        }

        btnRegister.setEnabled(false);

        currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        timestamp = currentTimeStamp.format(new Date());

//        etBirthday.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                final Calendar c = Calendar.getInstance();
//                year = c.get(Calendar.YEAR);
//                month = c.get(Calendar.MONTH);
//                day = c.get(Calendar.DAY_OF_MONTH);
//                DatePickerDialog datePickerDialog = new DatePickerDialog(FA_RegisterActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//                            @Override
//                            public void onDateSet(DatePicker view, int Cyear,
//                                                  int monthOfYear, int dayOfMonth) {
//                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
//                            }
//                        }, year, month, day);
//                int defaultyear = year-18;
//                datePickerDialog.updateDate(defaultyear, month, day);
//                datePickerDialog.show();
//            }
//        });

//        tlBday.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                final Calendar c = Calendar.getInstance();
//                year = c.get(Calendar.YEAR);
//                month = c.get(Calendar.MONTH);
//                day = c.get(Calendar.DAY_OF_MONTH);
//                DatePickerDialog datePickerDialog = new DatePickerDialog(FA_RegisterActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//                            @Override
//                            public void onDateSet(DatePicker view, int Cyear,
//                                                  int monthOfYear, int dayOfMonth) {
//                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
//                            }
//                        }, year, month, day);
//                int defaultyear = year-18;
//                datePickerDialog.updateDate(defaultyear, month, day);
//                datePickerDialog.show();
//            }
//        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FA_RegisterActivity.this, R.style.Dialog);
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_termsandconditions, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final Typeface headertextfont = Typeface.createFromAsset(getAssets(),  "fonts/Montserrat-Bold.otf");
                final Typeface subheadertextfont = Typeface.createFromAsset(getAssets(),  "fonts/Montserrat-Black.otf");
                final Typeface regulartextfont = Typeface.createFromAsset(getAssets(),  "fonts/Montserrat-Medium.otf");

                final TextView tvheader = (TextView) dialogView.findViewById(R.id.termsheader);
                final TextView tvsecondheader = (TextView) dialogView.findViewById(R.id.termssecondheader);
                final TextView tvthirdheader = (TextView) dialogView.findViewById(R.id.termsthirdheader);
                final TextView tvfourthheader = (TextView) dialogView.findViewById(R.id.termsfourthheader);
                final TextView tvfifthheader = (TextView) dialogView.findViewById(R.id.termsfifthheader);
                final TextView tvsixthheader = (TextView) dialogView.findViewById(R.id.termssixthheader);
                final TextView tvfirstp = (TextView) dialogView.findViewById(R.id.firstpar);
                final TextView tvsecondp = (TextView) dialogView.findViewById(R.id.secondpar);
                final TextView tvthirdp = (TextView) dialogView.findViewById(R.id.thirdpar);
                final TextView tvfourthp = (TextView) dialogView.findViewById(R.id.fourthpar);
                final TextView tvfifthp = (TextView) dialogView.findViewById(R.id.fifthpar);
                final TextView tvsixthp = (TextView) dialogView.findViewById(R.id.sixthpar);
                final FancyButton btnagree = (FancyButton) dialogView.findViewById(R.id.btnAgree);

                tvheader.setTypeface(headertextfont);
                tvsecondheader.setTypeface(headertextfont);
                tvthirdheader.setTypeface(headertextfont);
                tvfourthheader.setTypeface(headertextfont);
                tvfifthheader.setTypeface(headertextfont);
                tvsixthheader.setTypeface(headertextfont);

                tvfirstp.setTypeface(regulartextfont);
                tvsecondp.setTypeface(regulartextfont);
                tvthirdp.setTypeface(regulartextfont);
                tvfourthp.setTypeface(regulartextfont);
                tvfifthp.setTypeface(regulartextfont);
                tvsixthp.setTypeface(regulartextfont);

                tvfirstp.setText("These Terms and Conditions govern your relationship with MAGS DELIVERY" + "\n\n" +
                                 "Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service." + "\n\n" +
                                 "By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.");

                tvsecondp.setText("If you wish to purchase any product or service made available through the Service, you may be asked to supply certain information relevant to your Purchase including,your billing address, and your shipping information." + "\n\n" +
                                  "You represent and warrant that: (i)you supply to us is true, correct and complete." + "\n\n" +
                                  "By submitting such information, you grant us the right to provide the information to third parties for purposes of facilitating the completion of Purchases." + "\n\n" +
                                  "We reserve the right to refuse or cancel your order at any time for certain reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons. In the event that one or more of the items purchased is unavailable for any reason, a replacement item will be." + "\n\n" +
                                  "We reserve the right to refuse or cancel your order if fraud or an unauthorised or illegal transaction is suspected.");

                tvthirdp.setText("When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service." + "\n\n" +
                                 "You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.");

                tvfourthp.setText("The service and content and and functionality will remain the exclusive property of Mags Delivery. The Service is protected by copyright and trademark.");

                tvfifthp.setText("Your use of the Service is at your sole risk. The Service is provided on an \"AS IS\" and \"AS AVAILABLE\" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.");

                tvsixthp.setText("If you have any questions about these Terms, please contact us. MagsDelivery@gmail.com 09176727581");

                btnagree.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(FA_RegisterActivity.this, "You have agreed to the terms and conditions", Toast.LENGTH_SHORT).show();
                        cbterms.setChecked(true);
                        btnRegister.setEnabled(true);
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();
            }
        });

        ivCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(FA_RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Cyear,
                                                  int monthOfYear, int dayOfMonth) {
                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "-" + (dayOfMonth) + "-" + Cyear);
                            }
                        }, year, month, day);
                int defaultyear = year-18;
                datePickerDialog.updateDate(defaultyear, month, day);
                datePickerDialog.show();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FA_RegisterActivity.this, FA_LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        cbterms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((cbterms).isChecked()) {
                    btnRegister.setEnabled(true);
                }
                else{
                    btnRegister.setEnabled(false);
                }}
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerdialog = new MaterialDialog.Builder(FA_RegisterActivity.this)
                        .title("Signing Up")
                        .content("Please wait...")
                        .progress(true, 0)
                        .show();

                final String email = etEmail.getText().toString().trim();
                final String password = etPassword.getText().toString().trim();
                final String name = etUserId.getText().toString().trim();
                final String birthday = etBirthday.getText().toString().trim();
                final String address = etAddress.getText().toString().trim();
                final String contactnum = etContact.getText().toString().trim();
                isThisDateValid(etBirthday.getText().toString(),"MM-dd-yyyy");

                if (TextUtils.isEmpty(etUserId.getText().toString())) {
                    registerdialog.dismiss();
                    etUserId.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etBirthday.getText().toString())) {
                    registerdialog.dismiss();
                    etBirthday.setError("Required");
                    return;
                }
                if(dateformatstatus.equals("false")){
                    registerdialog.dismiss();
                    new MaterialDialog.Builder(FA_RegisterActivity.this)
                            .title("Register Failed")
                            .content("Date Format is not correct. Must be mm-dd-yyyy")
                            .positiveText("Close")
                            .show();
                    return;
                }
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    registerdialog.dismiss();
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    registerdialog.dismiss();
                    etContact.setError("Required");
                    return;
                }
                if (!isValidEmail(etEmail.getText().toString().trim())) {
                    registerdialog.dismiss();
                    etEmail.setError("Enter a valid Email Address");
                    return;
                }
                if (etPassword.length() < 6) {
                    registerdialog.dismiss();
                    etPassword.setError("Password must be atleast 6 characters long");
                    return;
                }
                if(age < 18){
                    registerdialog.dismiss();
                    new MaterialDialog.Builder(FA_RegisterActivity.this)
                            .title("Register Failed")
                            .content("You must be 18 or older to use the App!")
                            .positiveText("Close")
                            .show();
                }
                else {
                    if(intentstatus.equals("false")){
                        mAuth.createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener(FA_RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            C_UserClass user = new C_UserClass(name,address, contactnum, birthday, String.valueOf(age), email, "user", "none");
                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child("ORD-NAN").child("orderstatus").setValue("NaN");
                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child("ORD-NAN").child("ordernum").setValue("NaN");
                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child("ORD-NAN").child("scheduledateandtime").setValue("NaN");
                                            registerdialog.dismiss();
                                            Toast.makeText(FA_RegisterActivity.this, "Sign Up Successful!", Toast.LENGTH_SHORT).show();
                                            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("ordertransaction", "false");
                                            editor.putString("status", "user");
                                            editor.putString("userid", mAuth.getCurrentUser().getUid());
                                            editor.putString("username", name);
                                            editor.putString("useremail", email);
                                            editor.putString("useraddress", address);
                                            editor.putString("usercontact", contactnum);
                                            editor.putInt("timehldr",3000);
                                            editor.apply();
                                            Intent intent = new Intent(FA_RegisterActivity.this, U_MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            try {
                                                throw task.getException();
                                            } catch (FirebaseAuthUserCollisionException e) {
                                                new MaterialDialog.Builder(FA_RegisterActivity.this)
                                                        .title("Register Failed")
                                                        .content("Email already in use!")
                                                        .positiveText("Close")
                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                            @Override
                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                Intent intent = new Intent(FA_RegisterActivity.this, FA_RegisterActivity.class);
                                                                startActivity(intent);
                                                                finish();
                                                            }
                                                        })
                                                        .show();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                    }
                    else{
                        C_UserClass user = new C_UserClass(name,address, contactnum, birthday, String.valueOf(age), email, "user", "none");
                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child("ORD-NAN").child("orderstatus").setValue("NaN");
                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child("ORD-NAN").child("ordernum").setValue("NaN");
                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child("ORD-NAN").child("scheduledateandtime").setValue("NaN");
                        registerdialog.dismiss();
                        Toast.makeText(FA_RegisterActivity.this, "Sign Up Successful!", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                        editor.putString("ordertransaction", "false");
                        editor.putString("status", "user");
                        editor.putString("userid", intentuid);
                        editor.putString("username", name);
                        editor.putString("useremail", email);
                        editor.putString("useraddress", address);
                        editor.putString("usercontact", contactnum);
                        editor.putInt("timehldr",3000);
                        editor.apply();

                        Intent intent = new Intent(FA_RegisterActivity.this, U_MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public int getAge(EditText etBirthday){
        String parser = etBirthday.getText().toString();
        String delims = "[-]+";
        String[] tokens = parser.split(delims);

        int years = Integer.parseInt(tokens[2]);
        int yearCalculate = Calendar.getInstance().get(Calendar.YEAR);
        age = yearCalculate - years;
        return yearCalculate - years;
    }
    private static boolean editTextIsEmpty(EditText editText){
        return editText.getText().toString().isEmpty();
    }

    public boolean isThisDateValid(String dateToValidate, String dateFormat){
        if(dateToValidate == null){
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            Date date = sdf.parse(dateToValidate);
            getAge(etBirthday);
            dateformatstatus = "true";
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
