package androidstudio.beerexpresss;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JP on 11/27/2017.
 */

public class FA_HistoryAdapter extends BaseAdapter {
    ArrayList<C_OrderClass> items;
    Context c;

    public FA_HistoryAdapter(Context c, ArrayList<C_OrderClass> items) {
        this.c = c;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.orderhistoryrow, viewGroup, false);
        }

        final C_OrderClass o = (C_OrderClass) this.getItem(i);
        TextView tvid = (TextView) view.findViewById(R.id.orderid);
        TextView tvdate = (TextView) view.findViewById(R.id.orderdate);
        TextView tvtotal = (TextView) view.findViewById(R.id.ordertotal);

        Typeface headertextfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Montserrat-Bold.otf");
        Typeface regulartextfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Montserrat-Light.otf");

        tvid.setTypeface(headertextfont);
        tvdate.setTypeface(regulartextfont);
        tvtotal.setTypeface(regulartextfont);

        tvid.setText("Order ID: " +o.getOrdernum());
        tvtotal.setText("Order Status: " + o.getOrderstatus());

        if(o.getOrderstatus() != null) {
            if (o.getOrderstatus().equals("scheduled") || o.getOrderstatus().equals("scheduledelivered") || o.getOrderstatus().equals("scheduledeclined") || o.getOrderstatus().equals("scheduleaccepted") || o.getOrderstatus().equals("schedulecancelled")) {
                tvdate.setText("Scheduled Date & Time: " + o.getScheduledateandtime());
            } else {
                tvdate.setText("Order Timestamp: " + o.getTimestamp());
            }
        }
        return view;
    }
}