package androidstudio.beerexpresss;

/**
 * Created by JP on 11/23/2017.
 */


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 11/20/2017.
 */

public class U_MainFragment extends android.support.v4.app.Fragment implements FA_CartListener {


    //Preference
    private String USER_TYPE;
    String ordertransaction, uid, name, email, address, contact, deliverytype;
    String scheduledstatus = "false";
    String dateformatstatus = "false";
    String adjusteddate;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    //TextView
    @BindView(R.id.noteheader)
    TextView tvnoteheader;

    //GridView
    @BindView(R.id.gvitems)
    GridView gv;

    //TabLayout
    @BindView(R.id.tabs)
    TabLayout tbmenu;

    //Buttons
    @BindView(R.id.btnCart)
    FancyButton btnShowCart;
    @BindView(R.id.btnCheckout)
    FancyButton btnCheckout;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    ArrayList<C_MenuClass> menu;
    FA_CardAdapter adapter;
    FA_ListAdapter lvcartadapter;
    ArrayList<String> itemsarray;
    ArrayList<String> totalarray;
    Double carttotalval = 0.00;
    Double carttotalvalhldr = 0.00;
    Double originaltotalhldr = 0.00;
    String arrayvalhldr;
    String minimumcheck = "false";
    String coolerstatus = "false";
    public String timestamp;
    public SimpleDateFormat currentTimeStamp;

    //Location Variables
    String faddress;
    double flatitude, flongitude;
    Address geolocaddfromlatlong;
    LocationManager mLocationManager;
    Location myLocation;

    LocationManager locationManager;
    boolean GpsStatus = false;

    boolean cartdialog = false;
    boolean checkoutdialog = false;

    Geocoder geocoder;
    List<Address> addresses;
    String latitude, longitude;

    public int year, month, day, hour, minute;

    MaterialDialog minimum;

    public U_MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }


    public void Initialize() {
        checkLocationPermission();
        find_Location(getActivity());
        menu = new ArrayList<>();
        tbmenu.addTab(tbmenu.newTab().setText("Alcohol"), true);
        tbmenu.addTab(tbmenu.newTab().setText("Pulutan"));
        tbmenu.addTab(tbmenu.newTab().setText("Yosi"));
        tbmenu.addTab(tbmenu.newTab().setText("Pizza"));
        InitializeTabs();
        InitializeDesign();
        RetrieveUserData();

        if (ordertransaction.equals("true")) {
            Fragment fragment = new U_CurrentOrderFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment);
            fragmentTransaction.remove(new U_MainFragment());
            fragmentTransaction.commit();
        } else {
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();
            itemsarray = new ArrayList<>();
            totalarray = new ArrayList<>();
            adapter = new FA_CardAdapter(getActivity(), menu);
            adapter.setListener(this);
            getDataBeer();
            adapter.notifyDataSetChanged();
            gv.setAdapter(adapter);

            btnShowCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemsarray.size() != 0 && totalarray.size() != 0) {
                        CartManagement();
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Cart is empty!")
                                .content("Select Items first!")
                                .positiveText("Ok")
                                .show();
                    }
                }
            });

            btnCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Bold.otf");
                    final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Medium.otf");
                    final Typeface notetextfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Italic.otf");

                    if (itemsarray.size() != 0 && totalarray.size() != 0) {
                        carttotalval = 0.0;
                        GetCartTotal();
                        if (minimumcheck.equals("true")) {
                            CheckGpsStatus();
                            if (GpsStatus == true) {
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.popup_checkout, null);
                                dialogBuilder.setView(dialogView);

                                final AlertDialog alertDialog = dialogBuilder.create();
                                Window window = alertDialog.getWindow();
                                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                window.setGravity(Gravity.CENTER);

                                final ListView listView = (ListView) dialogView.findViewById(R.id.checkoutcartlist);
                                final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
                                final TextView tvsubheader1 = (TextView) dialogView.findViewById(R.id.subheader1);
                                final TextView tvsubheader2 = (TextView) dialogView.findViewById(R.id.subheader2);
                                final TextView textView = (TextView) dialogView.findViewById(R.id.checkoutcarttotal);
                                final TextInputLayout tladdress = (TextInputLayout) dialogView.findViewById(R.id.tladdress);
                                final TextInputLayout tlcontact = (TextInputLayout) dialogView.findViewById(R.id.tlcontact);
                                final TextInputLayout tlchange = (TextInputLayout) dialogView.findViewById(R.id.tlchangefor);
                                final TextInputLayout tllandmark = (TextInputLayout) dialogView.findViewById(R.id.tllandmark);
                                final TextInputLayout tladditional = (TextInputLayout) dialogView.findViewById(R.id.tladditional);
                                final EditText etaddress = (EditText) dialogView.findViewById(R.id.checkout_address);
                                final EditText etcontact = (EditText) dialogView.findViewById(R.id.checkout_contact);
                                final EditText etchange = (EditText) dialogView.findViewById(R.id.checkout_changefor);
                                final EditText etlandmark = (EditText) dialogView.findViewById(R.id.checkout_landmark);
                                final EditText etadditional = (EditText) dialogView.findViewById(R.id.checkout_additional);
                                final TextView tvcooler = (TextView) dialogView.findViewById(R.id.tvcooler);
                                final TextView tvnotice = (TextView) dialogView.findViewById(R.id.notice);
                                final TextView tvcoolernote = (TextView) dialogView.findViewById(R.id.coolernote);
                                final CheckBox checkboxrentalcooler = (CheckBox) dialogView.findViewById(R.id.checkout_cooler);
                                final TextView tvsubheader3 = (TextView) dialogView.findViewById(R.id.scheduledheader);
                                final TextView tvsubheader4 = (TextView) dialogView.findViewById(R.id.typeoforder);
                                final TextInputLayout tldate = (TextInputLayout) dialogView.findViewById(R.id.tldate);
                                final TextInputLayout tltime = (TextInputLayout) dialogView.findViewById(R.id.tltime);
                                final EditText etdate = (EditText) dialogView.findViewById(R.id.checkout_date);
                                final ImageView ivcalendar = (ImageView) dialogView.findViewById(R.id.ivcalendar);
                                final EditText ettime = (EditText) dialogView.findViewById(R.id.checkout_time);
                                final RadioGroup rgdelivery = (RadioGroup) dialogView.findViewById(R.id.rgdeltype);
                                final RadioButton rbnow = (RadioButton) dialogView.findViewById(R.id.rbnow);
                                final RadioButton rbsched = (RadioButton) dialogView.findViewById(R.id.rbscheduled);
                                final TextView tvcharge = (TextView) dialogView.findViewById(R.id.checkoutdeliverycharge);

                                tvheader.setTypeface(headertextfont);
                                textView.setTypeface(headertextfont);
                                tvsubheader1.setTypeface(regulartextfont);
                                tvsubheader2.setTypeface(regulartextfont);
                                tladdress.setTypeface(regulartextfont);
                                tlcontact.setTypeface(regulartextfont);
                                tlchange.setTypeface(regulartextfont);
                                tllandmark.setTypeface(regulartextfont);
                                tladditional.setTypeface(regulartextfont);
                                etaddress.setTypeface(regulartextfont);
                                etcontact.setTypeface(regulartextfont);
                                etchange.setTypeface(regulartextfont);
                                etlandmark.setTypeface(regulartextfont);
                                etadditional.setTypeface(regulartextfont);
                                tvcooler.setTypeface(regulartextfont);
                                tvsubheader3.setTypeface(regulartextfont);
                                tvsubheader4.setTypeface(regulartextfont);
                                tldate.setTypeface(regulartextfont);
                                tltime.setTypeface(regulartextfont);
                                etdate.setTypeface(regulartextfont);
                                ettime.setTypeface(regulartextfont);
                                rbnow.setTypeface(regulartextfont);
                                rbsched.setTypeface(regulartextfont);
                                tvnotice.setTypeface(notetextfont);
                                tvcoolernote.setTypeface(notetextfont);
                                tvcharge.setTypeface(regulartextfont);

                                carttotalvalhldr = carttotalval;
                                carttotalvalhldr = carttotalvalhldr + 70.00;

                                tvcharge.setText("Delivery Charge: ₱70.00");

                                checkboxrentalcooler.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Double withcooler;
                                        withcooler = carttotalvalhldr;
                                        originaltotalhldr = carttotalvalhldr;
                                        if ((checkboxrentalcooler).isChecked()) {
                                            coolerstatus = "true";
                                            if (Double.valueOf(withcooler) <= 1000.00) {
                                                withcooler = Double.valueOf(withcooler) + 40.00;
                                                textView.setText("Cart Total Value: ₱" + String.valueOf(withcooler));
                                                carttotalvalhldr = withcooler;
                                            }
                                        } else {
                                            coolerstatus = "false";
                                            withcooler = carttotalvalhldr;
                                            if (Double.valueOf(originaltotalhldr) <= 1000.00) {
                                                withcooler = Double.valueOf(withcooler) - 40.00;
                                                textView.setText("Cart Total Value: ₱" + String.valueOf(withcooler));
                                                carttotalvalhldr = withcooler;
                                            }
                                        }
                                    }
                                });

                                rbnow.setChecked(true);
                                deliverytype = "Now";
                                tldate.setEnabled(false);
                                etdate.setEnabled(false);
                                tltime.setEnabled(false);
                                ettime.setEnabled(false);
                                ivcalendar.setEnabled(false);

                                rgdelivery.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                        if (i == R.id.rbnow) {
                                            scheduledstatus = "false";
                                            deliverytype = "Now";
                                            tldate.setEnabled(false);
                                            etdate.setEnabled(false);
                                            tltime.setEnabled(false);
                                            ettime.setEnabled(false);
                                            ivcalendar.setEnabled(false);
                                        }
                                        if (i == R.id.rbscheduled) {
                                            scheduledstatus = "true";
                                            deliverytype = "Scheduled";
                                            tldate.setEnabled(true);
                                            etdate.setEnabled(true);
                                            tltime.setEnabled(true);
                                            ettime.setEnabled(true);
                                            ivcalendar.setEnabled(true);
                                        }
                                    }
                                });

                                ivcalendar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        final Calendar c = Calendar.getInstance();
                                        year = c.get(Calendar.YEAR);
                                        month = c.get(Calendar.MONTH);
                                        day = c.get(Calendar.DAY_OF_MONTH);
                                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                                            @Override
                                            public void onDateSet(DatePicker view, int Cyear,
                                                                  int monthOfYear, int dayOfMonth) {
                                                etdate.setText(/*"Birthday: " + */(monthOfYear + 1) + "-" + (dayOfMonth) + "-" + Cyear);
                                                tldate.clearFocus();
                                            }
                                        }, year, month, day);
                                        datePickerDialog.updateDate(year, month, day);
                                        datePickerDialog.show();
                                    }
                                });

                                tltime.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        final Calendar calendar = Calendar.getInstance();
                                        hour = calendar.get(Calendar.HOUR_OF_DAY);
                                        minute = calendar.get(Calendar.MINUTE);
                                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                                            @Override
                                            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                                                ettime.setText(String.format("%02d:%02d", i, i1));
                                                tltime.clearFocus();
                                            }
                                        }, hour, minute, true);
                                        timePickerDialog.show();
                                    }
                                });

                                ettime.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        final Calendar calendar = Calendar.getInstance();
                                        hour = calendar.get(Calendar.HOUR_OF_DAY);
                                        minute = calendar.get(Calendar.MINUTE);
                                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                                            @Override
                                            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                                                ettime.setText(String.format("%02d:%02d", i, i1));
                                                tltime.clearFocus();
                                            }
                                        }, hour, minute, true);
                                        timePickerDialog.show();
                                    }
                                });

                                etaddress.setText(address);
                                etcontact.setText(contact);

                                setListViewHeightBasedOnChildren(listView);
                                listView.setAdapter(new FA_ListAdapter(getActivity(), ViewCartList()));
                                listView.setOnTouchListener(new View.OnTouchListener() {
                                    // Setting on Touch Listener for handling the touch inside ScrollView
                                    @Override
                                    public boolean onTouch(View v, MotionEvent event) {
                                        // Disallow the touch request for parent scroll on touch of child view
                                        v.getParent().requestDisallowInterceptTouchEvent(true);
                                        return false;
                                    }
                                });

                                textView.setText("Cart Total Value: ₱" + String.valueOf(carttotalval));


                                FancyButton cancel_btn = (FancyButton) dialogView.findViewById(R.id.buttoncancel);
                                FancyButton order_btn = (FancyButton) dialogView.findViewById(R.id.buttonorder);
                                cancel_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });
                                order_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (scheduledstatus.equals("true")) {
                                            isThisDateValid(etdate.getText().toString(), "MM-dd-yyyy");
                                            if (TextUtils.isEmpty(etdate.getText().toString())) {
                                                etdate.setError("Required");
                                                return;
                                            }
                                            if (TextUtils.isEmpty(ettime.getText().toString())) {
                                                ettime.setError("Required");
                                                return;
                                            }
                                            if (dateformatstatus.equals("false")) {
                                                new MaterialDialog.Builder(getActivity())
                                                        .title("Scheduling Failed")
                                                        .content("Date Format is not correct. Must be mm-dd-yyyy")
                                                        .positiveText("Close")
                                                        .show();
                                                return;
                                            } else {
                                                adjustDateFormat(etdate);
                                            }
                                        }
                                        if (TextUtils.isEmpty(etaddress.getText().toString())) {
                                            etaddress.setError("Required");
                                            return;
                                        }
                                        if (TextUtils.isEmpty(etcontact.getText().toString())) {
                                            etcontact.setError("Required");
                                            return;
                                        }
                                        if (TextUtils.isEmpty(etchange.getText().toString())) {
                                            etchange.setError("Required");
                                            return;
                                        }
                                        if (Double.valueOf(etchange.getText().toString()) < carttotalval) {
                                            etchange.setError("Change is less than total");
                                            return;
                                        } else {
                                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                            LayoutInflater inflater = getActivity().getLayoutInflater();
                                            View dialogView = inflater.inflate(R.layout.popup_choice, null);
                                            dialogBuilder.setView(dialogView);

                                            final AlertDialog OptionalertDialog = dialogBuilder.create();
                                            Window window = OptionalertDialog.getWindow();
                                            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                            window.setGravity(Gravity.CENTER);

                                            final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                                            final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                                            final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                                            optionheader.setTypeface(headertextfont);
                                            optionheader.setText("Proceed now?");

                                            negative.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    OptionalertDialog.dismiss();
                                                }
                                            });

                                            positive.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Date currentTime = Calendar.getInstance().getTime();
                                                    DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
                                                    String reportDate = df.format(currentTime);
                                                    final int random = new Random().nextInt(991) + 10;
                                                    String ordernum = "ORD-" + uid.substring(20) + random;
                                                    C_OrderClass order;
                                                    String additionalinsthldr = etadditional.getText().toString();
                                                    String landmarkhldr = etlandmark.getText().toString();
                                                    if (additionalinsthldr.equals("") || additionalinsthldr.equals(" ")) {
                                                        additionalinsthldr = "None";
                                                    }
                                                    if (landmarkhldr.equals("") || landmarkhldr.equals(" ")) {
                                                        landmarkhldr = "None";
                                                    }

                                                    if (address.equals(etaddress.getText().toString())) {
                                                        getLastKnownLocation();
                                                        if (scheduledstatus.equals("false")) {
                                                            order = new C_OrderClass(ordernum, uid, etaddress.getText().toString(), etcontact.getText().toString(), name,
                                                                    reportDate, etchange.getText().toString(), String.valueOf(carttotalvalhldr),
                                                                    itemsarray, latitude, longitude, etaddress.getText().toString(), "pending", "ND", "ND", "ND", "ND",
                                                                    landmarkhldr, additionalinsthldr, deliverytype, "Now", coolerstatus);
                                                            mDatabase.child("Orders").child(ordernum).setValue(order);
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorderid").setValue(ordernum);
                                                        } else if (scheduledstatus.equals("true")) {
                                                            String dateandtime = adjusteddate + " " + ettime.getText().toString();
                                                            order = new C_OrderClass(ordernum, uid, etaddress.getText().toString(), etcontact.getText().toString(), name,
                                                                    reportDate, etchange.getText().toString(), String.valueOf(carttotalvalhldr),
                                                                    itemsarray, latitude, longitude, etaddress.getText().toString(), "scheduled", "ND", "ND", "ND", "ND",
                                                                    landmarkhldr, additionalinsthldr, deliverytype, dateandtime, coolerstatus);
                                                            mDatabase.child("Orders").child(ordernum).setValue(order);
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child(ordernum).child("ordernum").setValue(ordernum);
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child(ordernum).child("orderstatus").setValue("scheduled");
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child(ordernum).child("scheduledateandtime").setValue(dateandtime);
                                                        }
                                                    } else {
                                                        if (scheduledstatus.equals("false")) {
                                                            getLocationFromAddress(getActivity(), etaddress.getText().toString());
                                                            order = new C_OrderClass(ordernum, uid, String.valueOf(geolocaddfromlatlong), etcontact.getText().toString(), name,
                                                                    reportDate, etchange.getText().toString(), String.valueOf(carttotalvalhldr),
                                                                    itemsarray, latitude, longitude, etaddress.getText().toString(), "pending", "ND", "ND", "ND", "ND",
                                                                    landmarkhldr, additionalinsthldr, deliverytype, "Now", coolerstatus);
                                                            mDatabase.child("Orders").child(ordernum).setValue(order);
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorderid").setValue(ordernum);
                                                        } else if (scheduledstatus.equals("true")) {
                                                            String dateandtime = adjusteddate + " " + ettime.getText().toString();
                                                            order = new C_OrderClass(ordernum, uid, String.valueOf(geolocaddfromlatlong), etcontact.getText().toString(), name,
                                                                    reportDate, etchange.getText().toString(), String.valueOf(carttotalvalhldr),
                                                                    itemsarray, latitude, longitude, etaddress.getText().toString(), "scheduled", "ND", "ND", "ND", "ND",
                                                                    landmarkhldr, additionalinsthldr, deliverytype, dateandtime, coolerstatus);

                                                            mDatabase.child("Orders").child(ordernum).setValue(order);
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child(ordernum).child("ordernum").setValue(ordernum);
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child(ordernum).child("orderstatus").setValue("scheduled");
                                                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child(ordernum).child("scheduledateandtime").setValue(dateandtime);
                                                        }
                                                    }

                                                    if (scheduledstatus.equals("false")) {
                                                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                        if (address.equals(etaddress.getText().toString())) {
                                                            editor.putString("orderaddress", etaddress.getText().toString());
                                                        } else {
                                                            editor.putString("orderaddress", String.valueOf(geolocaddfromlatlong));
                                                        }
                                                        editor.putString("ordertransaction", "true");
                                                        editor.putString("orderid", ordernum);
                                                        editor.putString("ordercontact", etcontact.getText().toString());
                                                        editor.putString("ordertimestamp", reportDate);
                                                        editor.putString("orderchangefor", etchange.getText().toString());
                                                        editor.putString("nongeolocadd", etaddress.getText().toString());
                                                        editor.putString("orderlandmark", etlandmark.getText().toString());
                                                        editor.putString("orderadditional", etadditional.getText().toString());
                                                        editor.putString("orderlatitude", latitude);
                                                        editor.putString("orderlongitude", longitude);
                                                        editor.putString("orderstatus", "pending");
                                                        editor.putString("ordertotal", String.valueOf(carttotalvalhldr));
                                                        Set<String> set = new HashSet<String>();
                                                        set.addAll(itemsarray);
                                                        editor.putStringSet("orderitemarray", set);
                                                        editor.apply();

                                                        Toast.makeText(getActivity(), "Order sent!!", Toast.LENGTH_SHORT).show();
                                                        alertDialog.dismiss();
                                                        OptionalertDialog.dismiss();

                                                        Fragment fragment = new U_CurrentOrderFragment();
                                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                        fragmentTransaction.replace(R.id.frame_container, fragment);
                                                        fragmentTransaction.remove(new U_MainFragment());
                                                        fragmentTransaction.commit();

                                                    } else {
                                                        alertDialog.dismiss();
                                                        OptionalertDialog.dismiss();
                                                        coolerstatus = "false";
                                                        scheduledstatus = "false";
                                                        deliverytype = "Now";
                                                        tldate.setEnabled(false);
                                                        etdate.setEnabled(false);
                                                        tltime.setEnabled(false);
                                                        ettime.setEnabled(false);
                                                        ivcalendar.setEnabled(false);
                                                        itemsarray = new ArrayList<>();
                                                        totalarray = new ArrayList<>();

                                                        Toast.makeText(getActivity(), "Your order has been scheduled. Thank you!", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                            OptionalertDialog.setCancelable(false);
                                            OptionalertDialog.show();
                                        }
                                    }
                                });
                                alertDialog.setCancelable(false);
                                alertDialog.show();
                            } else {
                                Toast.makeText(getActivity(), "Location Services Is Disabled", Toast.LENGTH_SHORT).show();
                                displayPromptForEnablingGPS(getActivity());
                            }
                        }
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("Cart is empty!")
                                .content("Select Items first!")
                                .positiveText("Ok")
                                .show();
                    }
                }
            });
        }
    }

    private void getDataBeer() {
        C_MenuClass m = new C_MenuClass();
        m.setItemname("Red Horse 500ml");
        m.setItemprice("₱48.00");
        m.setItemimage(R.drawable.red_horse500);
        m.setItemtype("Beer");
        m.setItemcasequan(12.00);
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Red Horse Mucho");
        m.setItemprice("₱94.00");
        m.setItemimage(R.drawable.red_horsemucho);
        m.setItemtype("Beer");
        m.setItemcasequan(6.00);
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Pale Pilsen");
        m.setItemprice("₱37.00");
        m.setItemimage(R.drawable.pilsen);
        m.setItemtype("Beer");
        m.setItemcasequan(24.00);
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Pale Grande");
        m.setItemprice("₱94.00");
        m.setItemimage(R.drawable.grande);
        m.setItemtype("Beer");
        m.setItemcasequan(6.00);
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("San Mig Light 330ml");
        m.setItemprice("₱39.00");
        m.setItemimage(R.drawable.sanmiglight);
        m.setItemtype("Beer");
        m.setItemcasequan(24.00);
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Tanduay Ice 330ml");
        m.setItemprice("₱37.00");
        m.setItemimage(R.drawable.tanduayice);
        m.setItemtype("Beer");
        m.setItemcasequan(24.00);
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Tiger 330ml");
        m.setItemprice("₱47.00");
        m.setItemimage(R.drawable.tiger);
        m.setItemtype("Beer");
        m.setItemcasequan(12.00);
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Smirnoff Mule 330ml");
        m.setItemprice("₱44.00");
        m.setItemimage(R.drawable.smirnoffmule);
        m.setItemtype("Alcohol");
        menu.add(m);


        m = new C_MenuClass();
        m.setItemname("Budewesier 330ml");
        m.setItemprice("₱57.00");
        m.setItemimage(R.drawable.budweiser);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Fundador Solera 1L");
        m.setItemprice("₱599.00");
        m.setItemimage(R.drawable.fundadorsolera);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Fundador Light 1L");
        m.setItemprice("₱409.00");
        m.setItemimage(R.drawable.fundadorlight);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Gin Bilog");
        m.setItemprice("₱44.00");
        m.setItemimage(R.drawable.ginbilog);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Quatro Kintos");
        m.setItemprice("₱99.00");
        m.setItemimage(R.drawable.ginquatro);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Emperador 350ml");
        m.setItemprice("₱55.00");
        m.setItemimage(R.drawable.emperador350ml);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Emperador 500ml");
        m.setItemprice("₱70.00");
        m.setItemimage(R.drawable.emperador500ml);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Emperador 750ml");
        m.setItemprice("₱95.00");
        m.setItemimage(R.drawable.emperador750ml);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Emperador 1 Liter");
        m.setItemprice("₱125.00");
        m.setItemimage(R.drawable.emperador1lt);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Bacardi Superior 750ml");
        m.setItemprice("₱569.00");
        m.setItemimage(R.drawable.bacardi);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Red Label 1L");
        m.setItemprice("₱849.00");
        m.setItemimage(R.drawable.redlabel);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Black Label 1L");
        m.setItemprice("₱1299.00");
        m.setItemimage(R.drawable.blacklabel);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Double Black 1L");
        m.setItemprice("₱1949.00");
        m.setItemimage(R.drawable.doubleblack);
        m.setItemtype("Alcohol");
        menu.add(m);


        m = new C_MenuClass();
        m.setItemname("Jack Daniels 750 ml");
        m.setItemprice("₱1249.00");
        m.setItemimage(R.drawable.jackdaniels);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Jose Cuervo Gold 1L");
        m.setItemprice("₱1049.00");
        m.setItemimage(R.drawable.josecuervogold);
        m.setItemtype("Alcohol");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Jagermeister 700ml");
        m.setItemprice("₱899.00");
        m.setItemimage(R.drawable.jagermeister);
        m.setItemtype("Alcohol");
        menu.add(m);
    }

    private void getDataCrackers() {
        C_MenuClass m = new C_MenuClass();
        m.setItemname("Barbecue");
        m.setItemprice("₱13.00");
        m.setItemimage(R.drawable.bbq);
        m.setItemtype("BBQ");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Chippy 110g");
        m.setItemprice("₱30.00");
        m.setItemimage(R.drawable.chippy);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Nova 78g");
        m.setItemprice("₱35.00");
        m.setItemimage(R.drawable.nova);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("V-Cut 60g");
        m.setItemprice("₱35.00");
        m.setItemimage(R.drawable.vcut);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Pic-A 180g");
        m.setItemprice("₱49.00");
        m.setItemimage(R.drawable.pica);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Cheez-It 95g");
        m.setItemprice("₱25.00");
        m.setItemimage(R.drawable.cheezit);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Mr. Chips 100g");
        m.setItemprice("₱30.00");
        m.setItemimage(R.drawable.mrchips);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Tortillos 100g");
        m.setItemprice("₱30.00");
        m.setItemimage(R.drawable.tortillos);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Tempura 100g");
        m.setItemprice("₱25.00");
        m.setItemimage(R.drawable.tempura);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Piattos 85g");
        m.setItemprice("₱35.00");
        m.setItemimage(R.drawable.piattos);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("DingDong 100g");
        m.setItemprice("₱25.00");
        m.setItemimage(R.drawable.dingdong);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Boybawang 100g");
        m.setItemprice("₱25.00");
        m.setItemimage(R.drawable.boybawang);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Adobong Mani 250g");
        m.setItemprice("₱45.00");
        m.setItemimage(R.drawable.mani1);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Pringles");
        m.setItemprice("₱100.00");
        m.setItemimage(R.drawable.pringles);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Cheese Balls");
        m.setItemprice("₱85.00");
        m.setItemimage(R.drawable.cheeseballs);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Cheese Rings");
        m.setItemprice("₱85.00");
        m.setItemimage(R.drawable.cheeserings);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Cheezy Pops");
        m.setItemprice("₱85.00");
        m.setItemimage(R.drawable.cheezypops);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Fish Crackers");
        m.setItemprice("₱85.00");
        m.setItemimage(R.drawable.fishcracker);
        m.setItemtype("Chips");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Nachos");
        m.setItemprice("₱75.00");
        m.setItemimage(R.drawable.nachos);
        m.setItemtype("Chips");
        menu.add(m);

    }

    private void getDataYosi() {
        C_MenuClass m = new C_MenuClass();
        m.setItemname("Marlboro Red");
        m.setItemprice("₱99.00");
        m.setItemimage(R.drawable.marlborored);
        m.setItemtype("Yosi");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Marlboro Lights");
        m.setItemprice("₱99.00");
        m.setItemimage(R.drawable.white_menthol);
        m.setItemtype("Yosi");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Marlboro Blue");
        m.setItemprice("₱99.00");
        m.setItemimage(R.drawable.iceblast);
        m.setItemtype("Yosi");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Marlboro Black");
        m.setItemprice("₱99.00");
        m.setItemimage(R.drawable.black_menthol);
        m.setItemtype("Yosi");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Winston Red");
        m.setItemprice("₱89.00");
        m.setItemimage(R.drawable.winston_red);
        m.setItemtype("Yosi");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Winston Blue");
        m.setItemprice("₱89.00");
        m.setItemimage(R.drawable.winston_blue);
        m.setItemtype("Yosi");
        menu.add(m);
    }

    private void getDataPizza() {
        C_MenuClass m = new C_MenuClass();
        m.setItemname("Ham & Cheese(10x10)");
        m.setItemprice("₱79.00");
        m.setItemimage(R.drawable.pizza6);
        m.setItemtype("Pizza");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Hawaiian(10x10)");
        m.setItemprice("₱89.00");
        m.setItemimage(R.drawable.pizza1);
        m.setItemtype("Pizza");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Veggies(10x10)");
        m.setItemprice("₱89.00");
        m.setItemimage(R.drawable.pizza5);
        m.setItemtype("Pizza");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Pepperoni(10x10)");
        m.setItemprice("₱89.00");
        m.setItemimage(R.drawable.pizza3);
        m.setItemtype("Pizza");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Bacon(10x10)");
        m.setItemprice("₱99.00");
        m.setItemimage(R.drawable.pizza1);
        m.setItemtype("Pizza");
        menu.add(m);

        m = new C_MenuClass();
        m.setItemname("Beef & Mushroom(10x10)");
        m.setItemprice("₱119.00");
        m.setItemimage(R.drawable.pizza4);
        m.setItemtype("Pizza");
        menu.add(m);
    }


    private ArrayList ViewCartList() {
        ArrayList<C_MenuClass> listmenu = new ArrayList<>();
        C_MenuClass lm = new C_MenuClass();
        for (int i = 0; i < itemsarray.size(); i++) {
            lm = new C_MenuClass();
            arrayvalhldr = itemsarray.get(i);
            String parser = arrayvalhldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return listmenu;
    }

    public void GetCartTotal() {
        for (int c = 0; c < totalarray.size(); c++) {
            carttotalval = Double.parseDouble(totalarray.get(c)) + carttotalval;
        }
        if (carttotalval < 330) {
            if(minimum == null){
                minimum = new MaterialDialog.Builder(getActivity())
                        .title("Minimum Purchase of ₱330.00")
                        .content("Select more items")
                        .positiveText("Ok")
                        .show();
            }

        } else {
            minimumcheck = "true";
        }
    }

    @Override
    public void AddtoCart(String itemcart, String total) {
        itemsarray.add(itemcart);
        totalarray.add(total);
    }

    @Override
    public void UpdateCart(int index, String itemcart, String total) {
        itemsarray.set(index, itemcart);
        totalarray.set(index, total);
        if(lvcartadapter != null){
            lvcartadapter.notifyDataSetChanged();
        }
        adapter.notifyDataSetChanged();
        carttotalval = 0.0;
        GetCartTotal();
    }

    @Override
    public void RemovefromCart(int index) {
            itemsarray.remove(index);
            totalarray.remove(index);
            if(lvcartadapter != null) {
                lvcartadapter.remove(index);
                lvcartadapter.notifyDataSetChanged();
            }
            adapter.notifyDataSetChanged();
            carttotalval = 0.0;
            GetCartTotal();
    }

    @Override
    public void TriggerCartManagement(String validation) {
        if (validation.equals("true")) {
            CartManagement();
        }
    }

    public void RetrieveUserData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("ordertransaction", "order");
        uid = prefs.getString("userid", "UID");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
    }

    private void InitializeTabs() {
        tbmenu.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                if(menu.isEmpty()){
                    getDataBeer();
                    adapter.notifyDataSetChanged();
                }else{
                    menu.clear();
                    getDataBeer();
                    adapter.notifyDataSetChanged();
                }
                break;
            case 1:
                if(menu.isEmpty()){
                    getDataCrackers();
                    adapter.notifyDataSetChanged();
                }else{
                    menu.clear();
                    getDataCrackers();
                    adapter.notifyDataSetChanged();
                }
                break;
            case 2:
                if(menu.isEmpty()){
                    getDataYosi();
                    adapter.notifyDataSetChanged();
                }else{
                    menu.clear();
                    getDataYosi();
                    adapter.notifyDataSetChanged();
                }
                break;
            case 3:
                if(menu.isEmpty()){
                    getDataPizza();
                    adapter.notifyDataSetChanged();
                }else{
                    menu.clear();
                    getDataPizza();
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }


    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if(getActivity() != null) {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission Check");
                    Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    return null;
                } else {
                    Location l = mLocationManager.getLastKnownLocation(provider);
                    if (l == null) {
                        continue;
                    }
                    if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                        bestLocation = l;
                        latitude = String.valueOf(bestLocation.getLatitude());
                        longitude = String.valueOf(bestLocation.getLongitude());

                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        try {
                            addresses = geocoder.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();
                            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                            faddress = address;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
        return bestLocation;
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if(getActivity() != null) {
            if (locationManager != null) {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission Check");
                    Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    return null;
                } else {
                    try {
                        address = coder.getFromLocationName(strAddress, 5);
                        if (address == null) {
                            Toast.makeText(context, "Address is Invalid Try again!", Toast.LENGTH_SHORT).show();
                            return null;
                        }
                        geolocaddfromlatlong = address.get(0);
                        latitude = String.valueOf(geolocaddfromlatlong.getLatitude());
                        longitude = String.valueOf(geolocaddfromlatlong.getLongitude());

                        p1 = new LatLng(geolocaddfromlatlong.getLatitude(), geolocaddfromlatlong.getLongitude());
                        Toast.makeText(context, String.valueOf(p1), Toast.LENGTH_SHORT).show();

                    } catch (IOException ex) {

                        ex.printStackTrace();
                    }
                    return p1;
                }
            } else {
            }
        }
        return null;
    }

    public void find_Location(Context con) {
        String location_context = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) con.getSystemService(location_context);
        List<String> providers = locationManager.getProviders(true);
        for (String provider : providers) {
            if(getActivity() != null) {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission Check");
                    Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    return;
                } else {
                    locationManager.requestLocationUpdates(provider, 1000, 0,
                            new LocationListener() {
                                public void onLocationChanged(Location location) {
                                    getLastKnownLocation();
                                }

                                public void onProviderDisabled(String provider) {
                                }

                                public void onProviderEnabled(String provider) {
                                }

                                public void onStatusChanged(String provider, int status,
                                                            Bundle extras) {
                                }
                            });
                    Location location = locationManager.getLastKnownLocation(provider);
                    if (location != null) {
                        latitude = String.valueOf(location.getLatitude());
                        longitude = String.valueOf(location.getLongitude());
                    }
                }
            }
        }
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
        } else {
            //  request permission.
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                            android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[]
            permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (permissions.length == 1 &&
                    permissions[0] == android.Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(getActivity(), "Locationn permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void CartManagement(){
        carttotalval = 0.0;
        GetCartTotal();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popup_cart, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog alertDialog = dialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
        final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");

        final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
        final TextView tvcontent = (TextView) dialogView.findViewById(R.id.content);
        final ListView listView = (ListView) dialogView.findViewById(R.id.cartlist);
        final TextView textView = (TextView) dialogView.findViewById(R.id.carttotal);
        Button cancel_btn = (Button) dialogView.findViewById(R.id.buttoncancellist);

        tvheader.setTypeface(headertextfont);
        tvcontent.setTypeface(regulartextfont);
        textView.setTypeface(headertextfont);
        cancel_btn.setTypeface(regulartextfont);

        textView.setText("Cart Total Value: ₱" + String.valueOf(carttotalval));

        setListViewHeightBasedOnChildren(listView);
        lvcartadapter = new FA_ListAdapter(getActivity(), ViewCartList());
        listView.setAdapter(lvcartadapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                final Integer arraypositem = position;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_choice, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog OptionalertDialog = dialogBuilder.create();
                Window window = OptionalertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                optionheader.setTypeface(headertextfont);

                optionheader.setText("What would you like to do?");
                negative.setText("Delete Item");
                positive.setText("Update Item");

                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String itemnamehldr = itemsarray.get(arraypositem);
                        String parser = itemnamehldr;
                        String delims = "[/]+";
                        String[] tokens = parser.split(delims);
                        String itemcasehldr = String.valueOf(tokens[0]);
                        lvcartadapter.notifyDataSetInvalidated();
                        adapter.notifyDataSetInvalidated();

                        adapter.removeItem(arraypositem, itemcasehldr);
                        textView.setText("Cart Total Value: ₱" + String.valueOf(carttotalval));
                        OptionalertDialog.dismiss();
                    }
                });

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String itemnamehldr = itemsarray.get(arraypositem);
                        String parser = itemnamehldr;
                        String delims = "[/]+";
                        String[] tokens = parser.split(delims);
                        String itemcasehldr = String.valueOf(tokens[0]);
                        lvcartadapter.notifyDataSetInvalidated();
                        adapter.notifyDataSetInvalidated();

                        adapter.updateItem(arraypositem, itemcasehldr);
                        textView.setText("Cart Total Value: ₱" + String.valueOf(carttotalval));
                        OptionalertDialog.dismiss();
                        alertDialog.dismiss();
                    }
                });

                OptionalertDialog.show();
            }
        });

        // Cancel Button
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AppBarLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static void displayPromptForEnablingGPS(final Activity activity) {
        final AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "GPS/Location Setting must be enabled to continue";

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    public void InitializeDesign(){
        final Typeface notetextfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Italic.otf");
        tvnoteheader.setTypeface(notetextfont);

        ViewGroup vg = (ViewGroup) tbmenu.getChildAt(0);
        int tabsCount = vg.getChildCount();


        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    //Put your font in assests folder
                    //assign name of the font here (Must be case sensitive)
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Medium.otf"));
                }
            }
        }
    }

    public boolean isThisDateValid(String dateToValidate, String dateFormat){
        if(dateToValidate == null){
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            Date date = sdf.parse(dateToValidate);
            dateformatstatus = "true";

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String adjustDateFormat(EditText etdate){
        String parser = etdate.getText().toString();
        String delims = "[-]+";
        String[] tokens = parser.split(delims);

        int month = Integer.parseInt(tokens[0]);
        int day = Integer.parseInt(tokens[1]);
        int years = Integer.parseInt(tokens[2]);

        String newmonth = String.format("%02d", month);
        String newday =  String.format("%02d", day);
        adjusteddate = newmonth+"-"+newday+"-"+years;

        return adjusteddate;
    }

}

