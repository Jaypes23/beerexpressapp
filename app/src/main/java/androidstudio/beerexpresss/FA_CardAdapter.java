package androidstudio.beerexpresss;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 11/23/2017.
 */

public class FA_CardAdapter extends BaseAdapter {
    private Context c;
    private ArrayList<C_MenuClass> items;
    private ArrayList<String> itemshldr = new ArrayList<>();
    private ArrayList<String> totalhldr = new ArrayList<>();
    private FA_CartListener mListener;
    private String namehldr,quanhldr,pricehldr,casenumhldr;
    private int validationindex;
    private int posindex;
    private String validationstring = "false";
    private String itemfromarray;
    private String ifitemiscase = "false";
    private MaterialDialog bbqdialog;

    public FA_CardAdapter(Context c, ArrayList<C_MenuClass> items){
        this.c = c;
        this.items = items;
    }


    public void setListener(FA_CartListener listener){
        mListener = listener;
    }

    public void removeItem(int position, String searchQuery){
        posindex = position;
        if(posindex < itemshldr.size()) {
            String parser = itemshldr.get(posindex);
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemnamecompare = String.valueOf(tokens[0]);

            if (searchQuery.equals(itemnamecompare)) {
                itemshldr.remove(posindex);
                totalhldr.remove(posindex);
                mListener.RemovefromCart(posindex);
            }
        }
    }

    //------------------------------------------------UPDATE ITEM FROM CART------------------------------------------------//
    public void updateItem(int position, String updateitemname){
        posindex = position;
        String checkifcase = "Case of";
        //------------------------------------------------CHECK IF ITEM IS CASE OF BEER------------------------------------------------//
        if(updateitemname.toLowerCase().contains(checkifcase.toLowerCase()))   {
            ifitemiscase = "true";
        }else{
            ifitemiscase = "false";
        }
        if(posindex < itemshldr.size()) {
            String parser = itemshldr.get(posindex);
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemnamecompare = String.valueOf(tokens[0]);

            if (updateitemname.equals(itemnamecompare)) {
                validationstring = "true";
                namehldr = String.valueOf(tokens[0]);
                pricehldr = String.valueOf(tokens[1]);
                quanhldr = String.valueOf(tokens[2]);

                if (ifitemiscase.equals("true")) {
                    casenumhldr = String.valueOf(tokens[3]);
                }
                validationindex = posindex;
            }


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(c, R.style.Dialog);
            View dialogView = LayoutInflater.from(c).inflate(R.layout.popup_itemquantity, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog ItemQuanDialog = dialogBuilder.create();
            Window window = ItemQuanDialog.getWindow();
            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);

            final NumberPicker itemquanpicker = (NumberPicker) dialogView.findViewById(R.id.number_picker);
            final TextView itemquanheader = (TextView) dialogView.findViewById(R.id.header);
            final TextView itemquancontent = (TextView) dialogView.findViewById(R.id.content);
            final FancyButton btnNegative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
            final FancyButton btnPositive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

            Typeface headertextfont = Typeface.createFromAsset(c.getAssets(), "fonts/Montserrat-Bold.otf");
            Typeface regulartextfont = Typeface.createFromAsset(c.getAssets(), "fonts/Montserrat-Medium.otf");

            itemquanheader.setTypeface(headertextfont);
            itemquancontent.setTypeface(regulartextfont);
            itemquanpicker.setDividerColorResource(R.color.colorAccent);
            itemquanpicker.setMinValue(1);

            //------------------------------------------------IF ITEM EXISTS------------------------------------------------//
            if (validationstring.equals("true")) {
                itemquanheader.setText("Add to Cart");
                if (ifitemiscase.equals("true")) {
                    itemquancontent.setText(String.valueOf("Update Quantity (" + casenumhldr + " pcs)per case"));
                } else {
                    itemquancontent.setText("Update Quantity");
                }
                itemquanpicker.setValue(Integer.valueOf(quanhldr));
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pickerhldr = itemquanpicker.getValue();
                        //------------------------------------------------CHECK IF ITEM IS NOT CASE OF BEER------------------------------------------------//
                        if (ifitemiscase.equals("false")) {
                            String parser = pricehldr;
                            String delims = "[₱]+";
                            String[] pricetokens = parser.split(delims);
                            Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                            Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                            String itemtocart = namehldr + "/" + pricehldr + "/" + String.valueOf(pickerhldr);
                            mListener.UpdateCart(validationindex, itemtocart, String.valueOf(total));
                            itemshldr.set(validationindex, namehldr + "/" + pricehldr + "/" + String.valueOf(pickerhldr));
                            Toast.makeText(c, String.valueOf(pickerhldr) + " " + namehldr + " item quantity updated", Toast.LENGTH_SHORT).show();
                            validationstring = "false";
                            quanhldr = null;
                            ItemQuanDialog.dismiss();
                            mListener.TriggerCartManagement("true");
                            //------------------------------------------------CHECK IF ITEM IS CASE OF BEER------------------------------------------------//
                        } else if (ifitemiscase.equals("true")) {
                            String parser = String.valueOf(Double.parseDouble(pricehldr) / Double.parseDouble(casenumhldr));
                            Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                            Double total = (Double.parseDouble(casenumhldr) * Double.parseDouble(parser)) * Double.parseDouble(String.valueOf(quaninput));
                            pricehldr = String.valueOf(Double.valueOf(casenumhldr) * Double.parseDouble(parser));
                            String itemtocart = namehldr + "/" + pricehldr + "/" + String.valueOf(pickerhldr);
                            mListener.UpdateCart(validationindex, itemtocart, String.valueOf(total));
                            itemshldr.set(validationindex, namehldr + "/" + pricehldr + "/" + String.valueOf(pickerhldr) + "/" + casenumhldr);
                            Toast.makeText(c, String.valueOf(pickerhldr) + " " + namehldr + " item quantity updated", Toast.LENGTH_SHORT).show();
                            validationstring = "false";
                            quanhldr = null;
                            ItemQuanDialog.dismiss();
                        }
                    }
                });
                btnNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ItemQuanDialog.dismiss();
                    }
                });
            }

            ItemQuanDialog.show();
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.cardmodel, viewGroup, false);
        }

        final C_MenuClass m = (C_MenuClass) this.getItem(i);
        ImageView img = (ImageView) view.findViewById(R.id.itemimage);
        TextView tvname = (TextView) view.findViewById(R.id.itemname);
        TextView tvprice = (TextView) view.findViewById(R.id.itemprice);
        FancyButton btnAddCart = (FancyButton) view.findViewById(R.id.btnAddtoCart);

        final Typeface headertextfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Montserrat-Bold.otf");
        final Typeface regulartextfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Montserrat-Light.otf");

        tvname.setTypeface(regulartextfont);
        tvname.setTypeface(regulartextfont);
        tvprice.setTypeface(regulartextfont);

        img.setImageResource(m.getItemimage());
        tvname.setText(m.getItemname());
        tvprice.setText(m.getItemprice());

        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //------------------------------------------------IF ITEM IS BEER------------------------------------------------//
                if(m.getItemtype().equals("Beer")){
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(c, R.style.Dialog);
                    View dialogView = LayoutInflater.from(c).inflate(R.layout.popup_choice, null);
                    dialogBuilder.setView(dialogView);

                    final AlertDialog OptionalertDialog = dialogBuilder.create();
                    Window window = OptionalertDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER);

                    final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                    final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                    final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                    optionheader.setTypeface(headertextfont);
                    optionheader.setText("What would you like?");
                    negative.setText("Per Case");
                    positive.setText("Per Piece");

                    //------------------------------------------------IF USER SELECTS PER CASE(BEER)------------------------------------------------//
                    negative.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OptionalertDialog.hide();
                            OptionalertDialog.dismiss();

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(c, R.style.Dialog);
                            View dialogView = LayoutInflater.from(c).inflate(R.layout.popup_itemquantity, null);
                            dialogBuilder.setView(dialogView);

                            final AlertDialog ItemQuanDialog = dialogBuilder.create();
                            Window window = ItemQuanDialog.getWindow();
                            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            window.setGravity(Gravity.CENTER);

                           final NumberPicker itemquanpicker = (NumberPicker) dialogView.findViewById(R.id.number_picker);
                           final TextView itemquanheader = (TextView) dialogView.findViewById(R.id.header);
                           final TextView itemquancontent = (TextView) dialogView.findViewById(R.id.content);
                           final FancyButton btnNegative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                           final FancyButton btnPositive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                            itemquanheader.setTypeface(headertextfont);
                            itemquancontent.setTypeface(regulartextfont);

                            itemquanpicker.setDividerColorResource(R.color.colorAccent);

                            //------------------------------------------------CHECK IF CART IS EMPTY(BEER CASE)------------------------------------------------//
                            if(itemshldr.size() > 0) {
                                for (int x = 0; x < itemshldr.size(); x++) {
                                    itemfromarray = itemshldr.get(x);
                                    String parser = itemfromarray;
                                    String delims = "[/]+";
                                    String[] tokens = parser.split(delims);
                                    String itemcasehldr = "Case of "+m.getItemname();
                                    //------------------------------------------------CHECK IF ITEM SELECTED EXISTS IN CART(BEER CASE)------------------------------------------------//
                                    if (itemcasehldr.equals(String.valueOf(tokens[0]))) {
                                        validationstring = "true";
                                        namehldr = String.valueOf(tokens[0]);
                                        pricehldr = String.valueOf(tokens[1]);
                                        quanhldr = String.valueOf(tokens[2]);
                                        validationindex = x;
                                    }
                                }
                                //------------------------------------------------IF ITEM SELECTED EXISTS(BEER CASE)------------------------------------------------//
                                if(validationstring.equals("true")){
                                    itemquanheader.setText("Add to Cart");
                                    itemquancontent.setText(String.valueOf("Update Quantity ("+m.getItemcasequan())+" pcs)per case");
                                    itemquanpicker.setValue(Integer.valueOf(quanhldr));
                                    btnPositive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            int pickerhldr = itemquanpicker.getValue();
                                //------------------------------------------------IF USER SELECTS 0 IN QUANTITY(BEER CASE)------------------------------------------------//
                                            if (pickerhldr == 0) {
                                                itemshldr.remove(validationindex);
                                                totalhldr.remove(validationindex);
                                                mListener.RemovefromCart(validationindex);
                                                Toast.makeText(c, m.getItemname() + " removed from cart", Toast.LENGTH_SHORT).show();
                                                validationstring = "false";
                                                quanhldr = null;
                                //------------------------------------------------IF USER INPUTS QUANTITY(BEER CASE)------------------------------------------------//
                                            } else {
                                                String parser = String.valueOf(Double.parseDouble(pricehldr) / m.getItemcasequan());
                                                Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                                Double total = (m.getItemcasequan() * Double.parseDouble(parser)) * Double.parseDouble(String.valueOf(quaninput));
                                                pricehldr = String.valueOf(Double.valueOf(parser) * m.getItemcasequan());
                                                String itemtocart = namehldr + "/" + pricehldr + "/" + String.valueOf(pickerhldr);
                                                mListener.UpdateCart(validationindex, itemtocart, String.valueOf(total));
                                                itemshldr.set(validationindex, namehldr + "/" + pricehldr + "/" + String.valueOf(pickerhldr) + "/" + m.getItemcasequan());
                                                Toast.makeText(c, String.valueOf(pickerhldr) + " Cases " + m.getItemname() + " item quantity updated", Toast.LENGTH_SHORT).show();
                                                validationstring = "false";
                                                quanhldr = null;
                                                ItemQuanDialog.dismiss();
                                            }
                                        }
                                    });
                                 //------------------------------------------------IF ITEM SELECTED DOES NOT EXISTS(BEER CASE)------------------------------------------------//
                                }else{
                                    quanhldr = "0";
                                    itemquanheader.setText("Add to Cart");
                                    itemquancontent.setText(String.valueOf("Insert Quantity ("+m.getItemcasequan())+" pcs)per case");
                                    itemquanpicker.setValue(Integer.valueOf(quanhldr));
                                    btnPositive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            int pickerhldr = itemquanpicker.getValue();
                                            if(!(pickerhldr == 0)){
                                                String parser = m.getItemprice();
                                                String delims = "[₱]+";
                                                String[] pricetokens = parser.split(delims);
                                                Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                                Double total = (m.getItemcasequan() * Double.parseDouble(pricetokens[1])) * Double.parseDouble(String.valueOf(quaninput));
                                                Double caseprice = Double.valueOf(pricetokens[1]) * m.getItemcasequan();
                                                String itemtocart = "Case of "+m.getItemname()+"/"+caseprice+"/"+String.valueOf(pickerhldr);
                                                mListener.AddtoCart(itemtocart, String.valueOf(total));
                                                itemshldr.add("Case of "+m.getItemname()+"/"+caseprice+"/"+String.valueOf(pickerhldr) + "/" + m.getItemcasequan());
                                                totalhldr.add(String.valueOf(total));
                                                Toast.makeText(c, String.valueOf(pickerhldr) + " Cases of " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                                ItemQuanDialog.dismiss();
                                            }
                                        }
                                    });
                                }
                                //------------------------------------------------IF CART IS EMPTY(BEER CASE)------------------------------------------------//
                            }else {
                                quanhldr = "0";
                                itemquanheader.setText("Add to Cart");
                                itemquancontent.setText(String.valueOf("Insert Quantity ("+m.getItemcasequan())+" pcs)per case");
                                itemquanpicker.setValue(Integer.valueOf(quanhldr));
                                btnPositive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        int pickerhldr = itemquanpicker.getValue();
                                        if (!(pickerhldr == 0)) {
                                            String parser = m.getItemprice();
                                            String delims = "[₱]+";
                                            String[] pricetokens = parser.split(delims);
                                            Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                            Double total = (m.getItemcasequan() * Double.parseDouble(pricetokens[1])) * Double.parseDouble(String.valueOf(quaninput));
                                            Double caseprice = Double.valueOf(pricetokens[1]) * m.getItemcasequan();
                                            String itemtocart = "Case of " + m.getItemname() + "/" + caseprice + "/" + String.valueOf(pickerhldr);
                                            mListener.AddtoCart(itemtocart, String.valueOf(total));
                                            itemshldr.add("Case of " + m.getItemname() + "/" + caseprice + "/" + String.valueOf(pickerhldr) + "/" + m.getItemcasequan());
                                            totalhldr.add(String.valueOf(total));
                                            Toast.makeText(c, String.valueOf(pickerhldr) + " " + m.getItemname() + " added to cart", Toast.LENGTH_SHORT).show();
                                            ItemQuanDialog.dismiss();
                                        }
                                    }
                                });
                            }

                            btnNegative.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ItemQuanDialog.dismiss();
                                }
                            });
                            ItemQuanDialog.show();
                        }
                    });

                    //------------------------------------------------IF USER SELECTS PER PIECE(BEER)------------------------------------------------//
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OptionalertDialog.hide();
                            OptionalertDialog.dismiss();

                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(c, R.style.Dialog);
                            View dialogView = LayoutInflater.from(c).inflate(R.layout.popup_itemquantity, null);
                            dialogBuilder.setView(dialogView);

                            final AlertDialog ItemQuanDialog = dialogBuilder.create();
                            Window window = ItemQuanDialog.getWindow();
                            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            window.setGravity(Gravity.CENTER);

                            final NumberPicker itemquanpicker = (NumberPicker) dialogView.findViewById(R.id.number_picker);
                            final TextView itemquanheader = (TextView) dialogView.findViewById(R.id.header);
                            final TextView itemquancontent = (TextView) dialogView.findViewById(R.id.content);
                            final FancyButton btnNegative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                            final FancyButton btnPositive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                            itemquanheader.setTypeface(headertextfont);
                            itemquancontent.setTypeface(regulartextfont);


                            itemquanpicker.setDividerColorResource(R.color.colorAccent);
                            //------------------------------------------------CHECK IF CART IS EMPTY(BEER PIECE)------------------------------------------------//
                            if(itemshldr.size() > 0){
                                for(int x=0;x<itemshldr.size();x++){
                                    itemfromarray = itemshldr.get(x);
                                    String parser = itemfromarray;
                                    String delims = "[/]+";
                                    String[] tokens = parser.split(delims);
                                //------------------------------------------------CHECK IF ITEM SELECTED EXISTS IN CART(BEER PIECE)------------------------------------------------//
                                    if(m.getItemname().equals(String.valueOf(tokens[0]))){
                                        validationstring = "true";
                                        namehldr = String.valueOf(tokens[0]);
                                        pricehldr = String.valueOf(tokens[1]);
                                        quanhldr = String.valueOf(tokens[2]);
                                        validationindex = x;
                                    }
                                }
                                //------------------------------------------------IF ITEM SELECTED EXISTS(BEER PIECE)------------------------------------------------//
                                if(validationstring.equals("true")){
                                    itemquanheader.setText("Add to Cart");
                                    itemquancontent.setText("Update Quantity");
                                    itemquanpicker.setValue(Integer.valueOf(quanhldr));
                                    btnPositive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            int pickerhldr = itemquanpicker.getValue();
                                //------------------------------------------------IF USER SELECTS 0 IN QUANTITY(BEER PIECE)------------------------------------------------//
                                            if (pickerhldr == 0) {
                                                String parser = pricehldr;
                                                String delims = "[₱]+";
                                                String[] pricetokens = parser.split(delims);
                                                Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                                Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                                mListener.RemovefromCart(validationindex);
                                                itemshldr.remove(validationindex);
                                                totalhldr.remove(validationindex);
                                                Toast.makeText(c, m.getItemname() + " removed from cart" , Toast.LENGTH_SHORT).show();
                                                validationstring = "false";
                                                quanhldr = null;
                                                ItemQuanDialog.dismiss();
                                //------------------------------------------------IF USER INPUTS QUANTITY(BEER PIECE)------------------------------------------------//
                                            } else {
                                                String parser = pricehldr;
                                                String delims = "[₱]+";
                                                String[] pricetokens = parser.split(delims);
                                                Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                                Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                                String itemtocart = namehldr+"/"+pricehldr+"/"+String.valueOf(pickerhldr);
                                                mListener.UpdateCart(validationindex,itemtocart, String.valueOf(total));
                                                itemshldr.set(validationindex, namehldr+"/"+pricehldr+"/"+String.valueOf(pickerhldr));
                                                Toast.makeText(c, String.valueOf(pickerhldr) + " " + m.getItemname() + " item quantity updated" , Toast.LENGTH_SHORT).show();
                                                validationstring = "false";
                                                quanhldr = null;
                                                ItemQuanDialog.dismiss();
                                            }
                                        }
                                    });
                                    //------------------------------------------------IF ITEM SELECTED DOES NOT EXISTS(BEER PIECE)------------------------------------------------//
                                }else{
                                    quanhldr = "0";
                                    itemquanheader.setText("Add to Cart");
                                    itemquancontent.setText("Insert Quantity");
                                    itemquanpicker.setValue(Integer.valueOf(quanhldr));
                                    btnPositive.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            int pickerhldr = itemquanpicker.getValue();
                                            if(!(pickerhldr == 0)){
                                                String parser = m.getItemprice();
                                                String delims = "[₱]+";
                                                String[] pricetokens = parser.split(delims);
                                                Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                                Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                                String itemtocart = m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr);
                                                mListener.AddtoCart(itemtocart, String.valueOf(total));
                                                itemshldr.add(m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr));
                                                totalhldr.add(String.valueOf(total));
                                                Toast.makeText(c, String.valueOf(pickerhldr) + " " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                                ItemQuanDialog.dismiss();
                                            }
                                        }
                                    });
                                }
                                //------------------------------------------------IF CART IS EMPTY(BEER CASE)------------------------------------------------//
                            }else {
                                quanhldr = "0";
                                itemquanheader.setText("Add to Cart");
                                itemquancontent.setText("Insert Quantity");
                                itemquanpicker.setValue(Integer.valueOf(quanhldr));
                                btnPositive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        int pickerhldr = itemquanpicker.getValue();
                                        if (!(pickerhldr == 0)) {
                                            String parser = m.getItemprice();
                                            String delims = "[₱]+";
                                            String[] pricetokens = parser.split(delims);
                                            Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                            Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                            String itemtocart = m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr);
                                            mListener.AddtoCart(itemtocart, String.valueOf(total));
                                            itemshldr.add(m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr));
                                            totalhldr.add(String.valueOf(total));
                                            Toast.makeText(c, String.valueOf(pickerhldr) + " " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                            ItemQuanDialog.dismiss();
                                        }
                                    }
                                });
                            }

                            btnNegative.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ItemQuanDialog.dismiss();
                                }
                            });
                            ItemQuanDialog.show();
                        }
                    });
                    OptionalertDialog.show();
                //------------------------------------------------IF ITEM IS ALCOHOL/CHIPS/YOSI/PIZZA------------------------------------------------//
                } else if ((m.getItemtype().equals("Alcohol")) || (m.getItemtype().equals("Chips")) || (m.getItemtype().equals("Yosi")) || (m.getItemtype().equals("Pizza")) || (m.getItemtype().equals("BBQ")) ){
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(c, R.style.Dialog);
                    View dialogView = LayoutInflater.from(c).inflate(R.layout.popup_itemquantity, null);
                    dialogBuilder.setView(dialogView);

                    final AlertDialog ItemQuanDialog = dialogBuilder.create();
                    Window window = ItemQuanDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER);

                    final NumberPicker itemquanpicker = (NumberPicker) dialogView.findViewById(R.id.number_picker);
                    final TextView itemquanheader = (TextView) dialogView.findViewById(R.id.header);
                    final TextView itemquancontent = (TextView) dialogView.findViewById(R.id.content);
                    final FancyButton btnNegative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                    final FancyButton btnPositive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                    itemquanheader.setTypeface(headertextfont);
                    itemquancontent.setTypeface(regulartextfont);
                    itemquanpicker.setDividerColorResource(R.color.colorAccent);

                    if(m.getItemtype().equals("BBQ")){
                        bbqdialog = new MaterialDialog.Builder(c)
                                .title("Please Note")
                                .content("BBQs are available from 5-9pm ONLY")
                                .positiveText("Okay")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        ItemQuanDialog.show();
                                    }
                                })
                                .cancelable(false)
                                .show();
                    }else{
                        ItemQuanDialog.show();
                    }

                    //------------------------------------------------CHECK IF CART IS EMPTY(OTHER ITEMS)------------------------------------------------//
                    if(itemshldr.size() > 0){
                        for(int x=0;x<itemshldr.size();x++){
                            itemfromarray = itemshldr.get(x);
                            String parser = itemfromarray;
                            String delims = "[/]+";
                            String[] tokens = parser.split(delims);
                    //------------------------------------------------CHECK IF ITEM SELECTED EXISTS IN CART(OTHER ITEMS)------------------------------------------------//
                            if(m.getItemname().equals(String.valueOf(tokens[0]))){
                                validationstring = "true";
                                namehldr = String.valueOf(tokens[0]);
                                pricehldr = String.valueOf(tokens[1]);
                                quanhldr = String.valueOf(tokens[2]);
                                validationindex = x;
                            }
                        }
                    //------------------------------------------------IF ITEM SELECTED EXISTS IN CART(OTHER ITEMS)------------------------------------------------//
                        if(validationstring.equals("true")){
                            itemquanheader.setText("Add to Cart");
                            if(m.getItemtype().equals("BBQ")){
                                itemquancontent.setText("Update Quantity (*BBQ Available from 5-9pm only)");
                            }else{
                                itemquancontent.setText("Update Quantity");
                            }
                            itemquanpicker.setValue(Integer.valueOf(quanhldr));
                            btnPositive.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    int pickerhldr = itemquanpicker.getValue();
                    //------------------------------------------------IF USER SELECTS 0 IN QUANTITY(OTHER ITEMS)------------------------------------------------//
                                    if (pickerhldr == 0) {
                                        String parser = pricehldr;
                                        String delims = "[₱]+";
                                        String[] pricetokens = parser.split(delims);
                                        Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                        Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                        mListener.RemovefromCart(validationindex);
                                        itemshldr.remove(validationindex);
                                        totalhldr.remove(validationindex);
                                        Toast.makeText(c, m.getItemname() + " removed from cart" , Toast.LENGTH_SHORT).show();
                                        validationstring = "false";
                                        quanhldr = null;
                                        ItemQuanDialog.dismiss();
                    //------------------------------------------------IF USER INPUTS QUANTITY(OTHER ITEMS)------------------------------------------------//
                                    } else {
                                        String parser = pricehldr;
                                        String delims = "[₱]+";
                                        String[] pricetokens = parser.split(delims);
                                        Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                        Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                        String itemtocart = namehldr+"/"+pricehldr+"/"+String.valueOf(pickerhldr);
                                        mListener.UpdateCart(validationindex,itemtocart, String.valueOf(total));
                                        itemshldr.set(validationindex, namehldr+"/"+pricehldr+"/"+String.valueOf(pickerhldr));
                                        Toast.makeText(c, String.valueOf(pickerhldr) + " " + m.getItemname() + " item quantity updated" , Toast.LENGTH_SHORT).show();
                                        validationstring = "false";
                                        quanhldr = null;
                                        ItemQuanDialog.dismiss();
                                    }
                                }
                            });
                    //------------------------------------------------IF ITEM SELECTED DOES NOT EXISTS(OTHER ITEMS)------------------------------------------------//
                        }else{
                            quanhldr = "0";
                            itemquanheader.setText("Add to Cart");
                            if(m.getItemtype().equals("BBQ")){
                                itemquancontent.setText("Insert Quantity (*BBQ Available from 5-9pm only");
                            }else{
                                itemquancontent.setText("Insert Quantity");
                            }
                            itemquanpicker.setValue(Integer.valueOf(quanhldr));
                            btnPositive.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    int pickerhldr = itemquanpicker.getValue();
                                    if(!(pickerhldr == 0)){
                                        String parser = m.getItemprice();
                                        String delims = "[₱]+";
                                        String[] pricetokens = parser.split(delims);
                                        Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                        Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                        String itemtocart = m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr);
                                        mListener.AddtoCart(itemtocart, String.valueOf(total));
                                        itemshldr.add(m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr));
                                        totalhldr.add(String.valueOf(total));
                                        Toast.makeText(c, String.valueOf(pickerhldr) + " " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                        ItemQuanDialog.dismiss();
                                    }
                                }
                            });
                        }
                    //------------------------------------------------IF CART IS EMPTY(OTHER ITEMS)------------------------------------------------//
                    }else {
                        quanhldr = "0";
                        itemquanheader.setText("Add to Cart");
                        if(m.getItemtype().equals("BBQ")){
                            itemquancontent.setText("Insert Quantity (*BBQ Available from 5-9pm only");
                        }else{
                            itemquancontent.setText("Insert Quantity");
                        }
                        itemquanpicker.setValue(Integer.valueOf(quanhldr));
                        btnPositive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int pickerhldr = itemquanpicker.getValue();
                                if (!(pickerhldr == 0)) {
                                    String parser = m.getItemprice();
                                    String delims = "[₱]+";
                                    String[] pricetokens = parser.split(delims);
                                    Double quaninput = Double.parseDouble(String.valueOf(pickerhldr));
                                    Double total = Double.parseDouble(pricetokens[1]) * Double.parseDouble(String.valueOf(quaninput));
                                    String itemtocart = m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr);
                                    mListener.AddtoCart(itemtocart, String.valueOf(total));
                                    itemshldr.add(m.getItemname()+"/"+m.getItemprice()+"/"+String.valueOf(pickerhldr));
                                    totalhldr.add(String.valueOf(total));
                                    Toast.makeText(c, String.valueOf(pickerhldr) + " " + m.getItemname() + " added to cart" , Toast.LENGTH_SHORT).show();
                                    ItemQuanDialog.dismiss();
                                }
                            }
                        });
                    }

                    btnNegative.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ItemQuanDialog.dismiss();
                        }
                    });
                }
            }

        });

        return view;
    }
}
