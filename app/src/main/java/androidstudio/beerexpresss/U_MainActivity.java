package androidstudio.beerexpresss;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class U_MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbartitle;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    //Variables
    String ordertransaction,name,email,address,contact;
    String scheduledtoday, scheduledorderid;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    AlarmManager am;

    //Preference
    private String USER_TYPE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(U_MainActivity.this);
        am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        InitializeDesign();
        RetrieveUserData();
        CheckSystemMessages();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;
        int id = item.getItemId();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_beer) {
            if(ordertransaction.equals("false")){
                fragment = new U_MainFragment();
                toolbartitle.setText("Mags Delivery");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.replace(R.id.frame_container, fragment);
                transaction.commit();
            }
            else if(ordertransaction.equals("true")){
                fragment = new U_CurrentOrderFragment();
                toolbartitle.setText("Mags Delivery");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.replace(R.id.frame_container, fragment);
                transaction.commit();
            }
        }else if (id == R.id.nav_scheduled) {
            fragment = new U_ScheduledOrdersFragment();
            toolbartitle.setText("Scheduled");
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.frame_container, fragment);
            transaction.commit();
        }
        else if (id == R.id.nav_history) {
            fragment = new U_HistoryFragment();
            toolbartitle.setText("History");
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.frame_container, fragment);
            transaction.commit();
        } else if (id == R.id.nav_profile) {
            fragment = new FA_ProfileFragment();
            toolbartitle.setText("Profile");
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.replace(R.id.frame_container, fragment);
            transaction.commit();
        } else if(id == R.id.nav_logout){
            final FA_FirebaseClass FAFirebaseClass = new FA_FirebaseClass(U_MainActivity.this);
            FAFirebaseClass.getUserInstance().signOut();
            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
            editor.remove("status");
            editor.remove("timehldr");
            editor.remove("username");
            editor.remove("useremail");
            editor.remove("useraddress");
            editor.remove("usercontact");
            editor.remove("scheduledtoday");
            editor.remove("scheduledorderid");
            editor.apply();
            Intent intent = new Intent(U_MainActivity.this, FA_LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void RetrieveUserData(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("ordertransaction", "order");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
        checkScheduleStatus();
        RetrieveScheduledOrders();
    }

    public void checkScheduleStatus(){
        SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        scheduledtoday = prefs.getString("scheduledtoday", "schedule");
        scheduledorderid = prefs.getString("scheduledorderid", "id");

        if(scheduledtoday.equals("true")){
            RetrieveScheduledOrdersInfo();
            checkingOfScheduledOrders();
        }
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getAssets(),  "fonts/Montserrat-Bold.otf");
        toolbartitle.setTypeface(headerfont);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Medium.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new FA_CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void RetrieveScheduledOrders() {
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(this);
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        String idcheck = mAuth.getCurrentUser().getUid();
        if (!idcheck.isEmpty()) {
            mRootRef.child("Users")
                    .child(mAuth.getCurrentUser().getUid())
                    .child("ScheduledOrders")
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                            String orderidfromdb = order.getOrdernum();
                            String datefromdb = order.getScheduledateandtime();
                            String statusfromdb = order.getOrderstatus();
                            if (statusfromdb != null && datefromdb != null && orderidfromdb != null) {
                                if (statusfromdb.equals("scheduled")) {
                                    Date currentTime = Calendar.getInstance().getTime();
                                    DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
                                    String reportDate = df.format(currentTime);

                                    String parser = datefromdb;
                                    String delims = "[ ]+";
                                    String[] tokens = parser.split(delims);
                                    String dateandtime = String.valueOf(tokens[0]);

                                    if (dateandtime.equals(reportDate)) {
                                        if (scheduledtoday.equals("false")) {
                                            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("scheduledtoday", "true");
                                            editor.putString("scheduledorderid", orderidfromdb);

                                            Toast.makeText(U_MainActivity.this, "Scheduled Order today! Check Scheduled Orders Tab", Toast.LENGTH_SHORT).show();
                                            Context context = getApplicationContext();
                                            Intent notificationIntent = new Intent(context, U_MainActivity.class);
                                            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

                                            Notification myNotication;
                                            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                            Notification.Builder builder = new Notification.Builder(U_MainActivity.this);
                                            builder.setAutoCancel(true);
                                            builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                                            builder.setLights(Color.YELLOW, 3000, 3000);
                                            builder.setTicker("You have a Scheduled Delivery today! Order ID: " + orderidfromdb);
                                            builder.setContentTitle("Mags Delivery");
                                            builder.setContentText("You have a Scheduled Delivery today! Order ID: " + orderidfromdb);
                                            builder.setSmallIcon(R.drawable.magslogo);
                                            builder.setOngoing(false);
                                            builder.setContentIntent(intent);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                Bitmap Icon = BitmapFactory.decodeResource(getResources(), R.drawable.magslogo);
                                                builder.setLargeIcon(Icon);
                                            }
                                            builder.build();
                                            myNotication = builder.getNotification();
                                            manager.notify(11, myNotication);

                                            checkScheduleStatus();
                                        }
                                    }
                                }
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                            String orderidfromdb = order.getOrdernum();
                            String datefromdb = order.getScheduledateandtime();
                            String statusfromdb = order.getOrderstatus();

                            if (statusfromdb != null && datefromdb != null && orderidfromdb != null) {
                                if (statusfromdb.equals("scheduled")) {
                                    Date currentTime = Calendar.getInstance().getTime();
                                    DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
                                    String reportDate = df.format(currentTime);

                                    String parser = datefromdb;
                                    String delims = "[ ]+";
                                    String[] tokens = parser.split(delims);
                                    String dateandtime = String.valueOf(tokens[0]);

                                    if (dateandtime.equals(reportDate)) {
                                        if (scheduledtoday.equals("false")) {
                                            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("scheduledtoday", "true");
                                            editor.putString("scheduledorderid", orderidfromdb);

                                            Context context = getApplicationContext();
                                            Intent notificationIntent = new Intent(context, U_MainActivity.class);
                                            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

                                            Notification myNotication;
                                            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                            Notification.Builder builder = new Notification.Builder(U_MainActivity.this);
                                            builder.setAutoCancel(true);
                                            builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                                            builder.setLights(Color.YELLOW, 3000, 3000);
                                            builder.setTicker("You have a Scheduled Delivery today! Order ID: " + orderidfromdb);
                                            builder.setContentTitle("Mags Delivery");
                                            builder.setContentText("You have a Scheduled Delivery today! Order ID: " + orderidfromdb);
                                            builder.setSmallIcon(R.drawable.magslogo);
                                            builder.setOngoing(false);
                                            builder.setContentIntent(intent);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                Bitmap Icon = BitmapFactory.decodeResource(getResources(), R.drawable.magslogo);
                                                builder.setLargeIcon(Icon);
                                            }
                                            builder.build();
                                            myNotication = builder.getNotification();
                                            manager.notify(11, myNotication);

                                            checkScheduleStatus();
                                        }
                                    }
                                }

                            }
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
        }
    }

    public void RetrieveScheduledOrdersInfo(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(this);
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Users")
                .child(mAuth.getCurrentUser().getUid())
                .child("ScheduledOrders")
                .child(scheduledorderid)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        String orderidfromdb = order.getOrdernum();
                        String datefromdb = order.getScheduledateandtime();
                        String statusfromdb = order.getOrderstatus();

                        if (statusfromdb != null && datefromdb !=null && orderidfromdb !=null) {
                            if ((statusfromdb.equals("scheduleaccepted")) || (statusfromdb.equals("scheduledeclined")) || (statusfromdb.equals("scheduledelivered"))) {
                                if (scheduledtoday.equals("true")) {
                                    SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                    editor.putString("scheduledtoday", "false");
                                    editor.putString("scheduledorderid", "none");
                                }
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        String orderidfromdb = order.getOrdernum();
                        String datefromdb = order.getScheduledateandtime();
                        String statusfromdb = order.getOrderstatus();
                        if (statusfromdb != null && datefromdb !=null && orderidfromdb !=null) {
                            if ((statusfromdb.equals("scheduleaccepted")) || (statusfromdb.equals("scheduledeclined")) || (statusfromdb.equals("scheduledelivered"))) {
                                if (scheduledtoday.equals("true")) {
                                    SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                    editor.putString("scheduledtoday", "false");
                                    editor.putString("scheduledorderid", "none");
                                }
                            }
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });
    }

    public void CheckSystemMessages(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(this);
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Message")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_MessageClass message = dataSnapshot.getValue(C_MessageClass.class);
                        String msgheader = message.getMessageheader();
                        String msgcontent = message.getMessagecontent();
                        String msgstatus = message.getMessagestatus();

                        if(msgheader != null && msgcontent !=null && msgstatus !=null){
                            if(msgstatus.equals("Active")){
                                new MaterialDialog.Builder(U_MainActivity.this)
                                        .title(msgheader)
                                        .content(msgcontent)
                                        .positiveText("Noted")
                                        .show();
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        final C_MessageClass message = dataSnapshot.getValue(C_MessageClass.class);
                        String msgheader = message.getMessageheader();
                        String msgcontent = message.getMessagecontent();
                        String msgstatus = message.getMessagestatus();

                        if(msgheader != null && msgcontent !=null && msgstatus !=null){
                            if(msgstatus.equals("Active")){
                                new MaterialDialog.Builder(U_MainActivity.this)
                                        .title(msgheader)
                                        .content(msgcontent)
                                        .positiveText("Noted")
                                        .show();
                            }
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });
    }
     @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onDestroy() {super.onDestroy();}

    @Override
    public void onResume(){
        super.onResume();
        RetrieveScheduledOrders();
    }

    @Override
    public void onRestart(){
        super.onRestart();
        RetrieveScheduledOrders();
    }

    public void checkingOfScheduledOrders() {
        if(scheduledtoday.equals("true")) {
            Intent intent = new Intent(this, FA_ScheduledNotif.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), (60 * 60 * 1000), pendingIntent);
        }
    }
}



