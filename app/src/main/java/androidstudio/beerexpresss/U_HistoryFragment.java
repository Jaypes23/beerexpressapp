package androidstudio.beerexpresss;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 11/27/2017.
 */

public class U_HistoryFragment extends android.support.v4.app.Fragment {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    C_OrderClass lm;
    ArrayList<C_OrderClass> listmenu;
    ArrayList<String> orderdetails;
    ArrayList<ArrayList<String>> itemsfromhistoryarray;
    ArrayList<String> itemhldr;
    public String uid, orderid, orderdate, address, contact, total, changefor,status;
    public ArrayList<String> items;

    //UI
    @BindView(R.id.historylist)
    ListView orderlist;
    @BindView(R.id.header)
    TextView header;

    FA_HistoryAdapter itemAdapter;

    MaterialDialog dialog;

    public U_HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orderhistory, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
        header.setTypeface(headertextfont);
    }

    public void InitializeFunctions() {
        dialog = new MaterialDialog.Builder(getActivity())
                .title("Loading History")
                .content("Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        lm = new C_OrderClass();
        listmenu = new ArrayList<>();
        orderdetails = new ArrayList<>();
        itemsfromhistoryarray = new ArrayList<>();
        itemhldr = new ArrayList<>();
        items = new ArrayList<>();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        itemAdapter = new FA_HistoryAdapter(getActivity(), listmenu);
        orderlist.setAdapter(itemAdapter);

        RetrieveOrderHistory();

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_orderinfo, null);
                dialogBuilder.setView(dialogView);
                String valhldr = orderdetails.get(position);
                itemhldr = itemsfromhistoryarray.get(position);
                String stringtoparse = valhldr;
                String parser = stringtoparse;
                String delims = "[/]+";
                String[] tokens = parser.split(delims);
                String ordid = String.valueOf(tokens[0]);
                String orddate = String.valueOf(tokens[1]);
                String ordadd = String.valueOf(tokens[2]);
                String ordcon = String.valueOf(tokens[3]);
                String ordtotal = String.valueOf(tokens[4]);
                String ordchangefor = String.valueOf(tokens[5]);
                String ordstatus = String.valueOf(tokens[6]);

                final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
                final ListView listView = (ListView) dialogView.findViewById(R.id.deliveryitemlist);
                final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);

                final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
                final Typeface subheadertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Black.otf");
                final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");

                tvheader.setTypeface(headertextfont);
                tvordernum.setTypeface(regulartextfont);
                tvordtotal.setTypeface(regulartextfont);
                tvordstatus.setTypeface(regulartextfont);
                tvordaddress.setTypeface(regulartextfont);
                tvordcontact.setTypeface(regulartextfont);
                tvordchange.setTypeface(regulartextfont);
                tvordtimestamp.setTypeface(regulartextfont);

                tvordernum.setText("Order ID: " + ordid);
                tvordtotal.setText("Order Total: " + ordtotal);
                tvordstatus.setText("Order Status: " + ordstatus);
                tvordaddress.setText("Your Delivery Address: " + ordadd);
                tvordcontact.setText("Your Contact Number: " + ordcon);
                tvordchange.setText("Requested Change For: " + ordchangefor);
                tvordtimestamp.setText("Delivery Timestamp: " + orddate);

                setListViewHeightBasedOnChildren(listView);
                listView.setAdapter(new FA_ListAdapter(getActivity(), ViewCartList()));
                listView.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                // Cancel Button
                FancyButton cancel_btn = (FancyButton) dialogView.findViewById(R.id.buttoncancellist);
                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.hide();
                    }
                });

                alertDialog.show();
            }
        });
        dialog.dismiss();
    }


    public void RetrieveOrderHistory(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Orders")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        uid = order.getUid();
                        orderid = order.getOrdernum();
                        orderdate = order.getTimestamp();
                        address = order.getAddress();
                        contact = order.getContactnum();
                        total = order.getCarttotal();
                        changefor = order.getChangefor();
                        items = order.getItems();
                        status = order.getOrderstatus();

                        if(uid !=null && orderid !=null && orderdate !=null && address !=null && contact !=null && total !=null && changefor !=null && items !=null && status !=null) {
                            if (mAuth.getCurrentUser().getUid().equals(uid)) {
                                if (status.equals("scheduled")) {
                                    status = "Scheduled Order";
                                }
                                if (status.equals("scheduleaccepted")) {
                                    status = "Scheduled Order Accepted";
                                }
                                if (status.equals("scheduledeclined")) {
                                    status = "Scheduled Order Declined";
                                }
                                if (status.equals("scheduledelivered")) {
                                    status = "Scheduled Order Delivered";
                                }
                                if(status.equals("schedulecancelled")){
                                    status = "Scheduled Order Cancelled";
                                }
                                if (status.equals("otw")) {
                                    status = "Rider on the way";
                                }

                                if (status.equals("declined")) {
                                    status = "Order Cancelled";
                                }
                                if (status.equals("delivered")) {
                                    status = "Order Successfully Delivered";
                                }

                                lm = new C_OrderClass();
                                lm.setOrdernum(orderid);
                                lm.setTimestamp(orderdate);
                                lm.setAddress(address);
                                lm.setContactnum(contact);
                                lm.setCarttotal(total);
                                lm.setChangefor(changefor);
                                lm.setOrderstatus(status);
                                lm.setItems(items);
                                listmenu.add(lm);
                                orderdetails.add(orderid + "/" + orderdate + "/" + address + "/" + contact + "/" + total + "/" + changefor + "/" + status);
                                itemsfromhistoryarray.add(items);
                                itemAdapter.notifyDataSetChanged();
                            }
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {}

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }


//    class RetrieveOrderHistory extends AsyncTask<Void, Void, String> {
//        @Override
//        protected String doInBackground(Void... voids) {
//            FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
//            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
//            mRootRef.child("Orders")
//                    .addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            for (final DataSnapshot idsnapshot : dataSnapshot.getChildren()) {
//                                mRootRef.child("Orders")
//                                        .child(idsnapshot.getKey())
//                                        .addListenerForSingleValueEvent(new ValueEventListener() {
//                                            @Override
//                                            public void onDataChange(DataSnapshot idsnapshot) {
//                                                final C_OrderClass order = idsnapshot.getValue(C_OrderClass.class);
//                                                uid = order.getUid();
//                                                orderid = order.getOrdernum();
//                                                orderdate = order.getTimestamp();
//                                                address = order.getAddress();
//                                                contact = order.getContactnum();
//                                                total = order.getCarttotal();
//                                                changefor = order.getChangefor();
//                                                items = order.getItems();
//                                                if(mAuth.getCurrentUser().getUid().equals(uid)){
//                                                    lm = new C_OrderClass();
//                                                    isEmpty = "false";
//                                                    Toast.makeText(getActivity(), orderid, Toast.LENGTH_SHORT).show();
//                                                    lm.setOrdernum(orderid);
//                                                    lm.setTimestamp(orderdate);
//                                                    lm.setAddress(address);
//                                                    lm.setContactnum(contact);
//                                                    lm.setCarttotal(total);
//                                                    lm.setChangefor(changefor);
//                                                    lm.setItems(items);
//                                                    listmenu.add(lm);
//                                                    orderdetails.add(orderid+"/"+orderdate+"/"+address+"/"+contact+"/"+total+"/"+changefor);
//                                                    itemsfromhistoryarray.add(items);
//                                                }
//                                                else{
//                                                    isEmpty = "empty";
//                                                }
//                                            }
//
//                                            @Override
//                                            public void onCancelled(DatabaseError databaseError) {
//
//                                            }
//                                        });
//                            }
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//                        }
//                    });
//            return null;
//        }
//    }

    private ArrayList ViewCartList(){
        ArrayList<C_MenuClass> listmenu = new ArrayList<>();
        C_MenuClass lm = new C_MenuClass();
        for(int i = 0; i<itemhldr.size();i++){
            lm = new C_MenuClass();
            String arrayvalhldr = itemhldr.get(i);
            String parser = arrayvalhldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AppBarLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
