package androidstudio.beerexpresss;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

public class FA_LoginActivity extends AppCompatActivity {


    //TextInputLayout
    @BindView(R.id.login_usernametextinput)
    TextInputLayout tlemail;
    @BindView(R.id.login_passwordtextinput)
    TextInputLayout tlpassword;

    //EditText
    @BindView(R.id.login_username)
    EditText etUserId;
    @BindView(R.id.login_password)
    EditText etPassword;

    //Button
    @BindView(R.id.btnLogin)
    FancyButton btnLogin;
    @BindView(R.id.btnSignUp)
    FancyButton btnSignUp;
    @BindView(R.id.facebookLogin)
    FancyButton btnFB;

    //TextView
    @BindView(R.id.fbheader)
    TextView fbheader;

    //Variables
    private String userstatus,usertype, username, useremail, useraddress, usercontactnum, usercurrentorderid;
    private String fbuid, fbname, fbemail;
    private String uid,orderid,orderdate,address,contact,total,changefor,status,landmark,additional,latitude,longitude, geoaddress;
    private ArrayList<String> items;

    //Preference
    private String USER_TYPE;
    private String statushldr;
    private int timehldr;

    //MaterialDialog
    private MaterialDialog logindialog,fblogindialog;

    //Firebase
    private FA_FirebaseClass firebasefunctions;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    private CallbackManager callbackManager;
    public static final String TAG = "LoginActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        InitializeDesign();
        InitializeFunctions();
    }


    public void InitializeDesign(){
        Typeface regulartextfont = Typeface.createFromAsset(getAssets(),  "fonts/Montserrat-Medium.otf");
        fbheader.setTypeface(regulartextfont);
        tlemail.setTypeface(regulartextfont);
        tlpassword.setTypeface(regulartextfont);
        etUserId.setTypeface(regulartextfont);
        etPassword.setTypeface(regulartextfont);
    }

    public void InitializeFunctions(){
        mAuth = FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(FA_LoginActivity.this, "Facebook Authentication Successful! Please wait..", Toast.LENGTH_SHORT).show();
                fblogindialog = new MaterialDialog.Builder(FA_LoginActivity.this)
                        .title("Validating data")
                        .content("Please wait...")
                        .progress(true, 0)
                        .show();
                FBLogin(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(FA_LoginActivity.this, "FB Login Cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(FA_LoginActivity.this, String.valueOf(exception), Toast.LENGTH_SHORT).show();
            }
        });

        btnFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager
                        .getInstance().
                        logInWithReadPermissions(FA_LoginActivity.this, Arrays.asList("public_profile", "email"));
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FA_LoginActivity.this, FA_RegisterActivity.class);
                intent.putExtra("user_id","default");
                intent.putExtra("user_name", "default");
                intent.putExtra("user_email", "default");
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logindialog = new MaterialDialog.Builder(FA_LoginActivity.this)
                        .title("Logging in")
                        .content("Please wait...")
                        .progress(true, 0)
                        .show();
                if (TextUtils.isEmpty(etUserId.getText().toString())) {
                    etUserId.setError("Enter email address");
                    logindialog.dismiss();
                } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    etPassword.setError("Enter password");
                    logindialog.dismiss();
                } else if (!isValidEmail(etUserId.getText().toString())) {
                    etUserId.setError("Enter a valid email address");
                    logindialog.dismiss();
                } else {
                    mAuth.signInWithEmailAndPassword(etUserId.getText().toString(), etPassword.getText().toString())
                            .addOnCompleteListener(FA_LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull final Task<AuthResult> task) {
                                    if (!task.isSuccessful()) {
                                        logindialog.dismiss();
                                        try {
                                            throw task.getException();

                                        } catch (FirebaseAuthInvalidUserException e) {
                                            new MaterialDialog.Builder(FA_LoginActivity.this)
                                                    .title("Login Failed")
                                                    .content("User Not Found")
                                                    .positiveText("Close")
                                                    .show();
                                            etPassword.setText("");
                                        } catch (FirebaseAuthInvalidCredentialsException e) {
                                            new MaterialDialog.Builder(FA_LoginActivity.this)
                                                    .title("Login Failed")
                                                    .content("Email or Password did not match. Please try again")
                                                    .positiveText("Close")
                                                    .show();
                                            etPassword.setText("");
                                        } catch (FirebaseNetworkException e) {
                                            new MaterialDialog.Builder(FA_LoginActivity.this)
                                                    .title("Login Failed")
                                                    .content("Please Check Internet Connection, and try again")
                                                    .positiveText("Close")
                                                    .show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        //LOGIN SUCCESSFUL
                                        logindialog.dismiss();
                                        new RetrieveUserStatus().execute();
                                    }
                                }
                            });
                }
            }
        });

        firebasefunctions = new FA_FirebaseClass(this);
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = firebasefunctions.getUserInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
                statushldr = prefs.getString("status", "Welcome");
                timehldr = prefs.getInt("timehldr", 0);
                if(timehldr == 0){
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
                            statushldr = prefs.getString("status", "Welcome");
                            if(statushldr.equals("user")){
                                Intent intent = new Intent(FA_LoginActivity.this, U_MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            if(statushldr.equals("client")){
                                Intent intent = new Intent(FA_LoginActivity.this, R_MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            if(statushldr.equals("Welcome")){
                                Log.w("Login", "Welcome");
                            }
                        }
                    }, 3000);
                }

                else{
                    if(statushldr.equals("user")){
                        Intent intent = new Intent(FA_LoginActivity.this, U_MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    if(statushldr.equals("client")){
                        Intent intent = new Intent(FA_LoginActivity.this, R_MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    if(statushldr.equals("Welcome")){
                        Log.w("Login", "Welcome");
                    }
                }
            }
        };
    }

    public boolean isValidEmail(CharSequence target) {
        return target != null && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    class RetrieveUserStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(FA_LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final C_UserClass user = dataSnapshot.getValue(C_UserClass.class);
                                    userstatus = user.getUserstatus();
                                    username = user.getName();
                                    useremail = user.getEmail();
                                    useraddress = user.getAddress();
                                    usercontactnum = user.getContact();
                                    usercurrentorderid = user.getCurrentorderid();
                                    Toast.makeText(FA_LoginActivity.this, userstatus + username + useremail + useraddress + usercontactnum + usercurrentorderid, Toast.LENGTH_SHORT).show();

                                    if(userstatus != null && username != null && useremail != null && useraddress != null
                                            && usercontactnum != null && usercurrentorderid != null){
                                        if(userstatus.equals("user")){
                                            usertype = "user";
                                            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("ordertransaction", "false");
                                            editor.putString("scheduledtoday", "false");
                                            editor.putString("userid", mAuth.getCurrentUser().getUid());
                                            editor.putString("status", "user");
                                            editor.putString("username", username);
                                            editor.putString("useremail", useremail);
                                            editor.putString("useraddress", useraddress);
                                            editor.putString("usercontact", usercontactnum);
                                            editor.putInt("timehldr",3000);

                                            if(!usercurrentorderid.equals("none")){
                                                RetrieveCurrentOrderInfo();
                                            }

                                            editor.apply();
                                        } else if (userstatus.equals("client")){
                                            usertype = "client";
                                            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("ordertransaction", "false");
                                            editor.putString("userid", mAuth.getCurrentUser().getUid());
                                            editor.putString("status", "client");
                                            editor.putString("username", username);
                                            editor.putString("useremail", useremail);
                                            editor.putString("useraddress", useraddress);
                                            editor.putString("usercontact", usercontactnum);
                                            editor.putInt("timehldr",3000);
                                            editor.apply();
                                        }
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void FBLogin(AccessToken token){
        final AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            fbuid = user.getUid();
                            fbname = user.getDisplayName();
                            fbemail = user.getEmail();
                            new RetrieveUserStatusFB().execute();

                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(FA_LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            try {
                                throw task.getException();

                            } catch (FirebaseAuthUserCollisionException e) {
                                new MaterialDialog.Builder(FA_LoginActivity.this)
                                        .title("Login Failed")
                                        .content("An account already exists with the same email address. Try Sign in using email address")
                                        .positiveText("Close")
                                        .show();
                                etPassword.setText("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    class RetrieveUserStatusFB extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(FA_LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.hasChild(fbuid)){
                        mRootRef.child("Users")
                                .child(fbuid)
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final C_UserClass user = dataSnapshot.getValue(C_UserClass.class);
                                        userstatus = user.getUserstatus();
                                        username = user.getName();
                                        useremail = user.getEmail();
                                        useraddress = user.getAddress();
                                        usercontactnum = user.getContact();
                                        usercurrentorderid = user.getCurrentorderid();

                                        if (userstatus != null && username != null && useremail != null && useraddress != null
                                                && usercontactnum != null && usercurrentorderid != null) {
                                            if (userstatus.equals("user")) {
                                                usertype = "user";
                                                SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                editor.putString("ordertransaction", "false");
                                                editor.putString("scheduledtoday", "false");
                                                editor.putString("status", "user");
                                                editor.putString("userid", fbuid);
                                                editor.putString("username", username);
                                                editor.putString("useremail", useremail);
                                                editor.putString("useraddress", useraddress);
                                                editor.putString("usercontact", usercontactnum);
                                                editor.putInt("timehldr", 3000);
                                                editor.apply();

                                                if (!usercurrentorderid.equals("none")) {
                                                    RetrieveCurrentOrderInfo();
                                                }

                                                fblogindialog.dismiss();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }
                    else{
                        fblogindialog.dismiss();
                        Intent intent = new Intent(getApplicationContext(), FA_RegisterActivity.class);
                        intent.putExtra("user_id",fbuid);
                        intent.putExtra("user_name", fbname);
                        intent.putExtra("user_email", fbemail);
                        startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    public void RetrieveCurrentOrderInfo(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(this);
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Orders")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        uid = order.getUid();
                        orderid = order.getOrdernum();
                        orderdate = order.getTimestamp();
                        address = order.getAddress();
                        contact = order.getContactnum();
                        total = order.getCarttotal();
                        changefor = order.getChangefor();
                        items = order.getItems();
                        status = order.getOrderstatus();
                        additional = order.getAdditional();
                        landmark = order.getLandmark();
                        address = order.getRealaddress();
                        geoaddress = order.getAddress();
                        latitude = order.getLatitude();
                        longitude = order.getLongitude();

                        if(uid !=null && orderid !=null && orderdate !=null && address !=null && contact !=null && total !=null && changefor !=null
                                && items !=null && status !=null && additional !=null && landmark !=null && address !=null && geoaddress !=null
                                && latitude !=null && longitude !=null) {
                            if (orderid.equals(usercurrentorderid)) {
                                SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                editor.putString("ordertransaction", "true");
                                editor.putString("orderaddress", geoaddress);
                                editor.putString("orderid", orderid);
                                editor.putString("ordercontact", contact);
                                editor.putString("ordertimestamp", orderdate);
                                editor.putString("orderchangefor", changefor);
                                editor.putString("nongeolocadd", address);
                                editor.putString("orderlandmark", landmark);
                                editor.putString("orderadditional", additional);
                                editor.putString("orderlatitude", latitude);
                                editor.putString("orderlongitude", longitude);
                                editor.putString("orderstatus", status);
                                editor.putString("ordertotal", total);
                                Set<String> set = new HashSet<String>();
                                set.addAll(items);
                                editor.putStringSet("orderitemarray", set);
                                editor.apply();
                            }
                        }

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {}

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }


}