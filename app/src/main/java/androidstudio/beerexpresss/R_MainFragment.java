package androidstudio.beerexpresss;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.NOTIFICATION_SERVICE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 12/1/2017.
 */

public class R_MainFragment extends android.support.v4.app.Fragment {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    String USER_TYPE;
    C_OrderClass lm;
    ArrayList<C_OrderClass> listmenu;
    ArrayList<String> orderdetails;
    ArrayList<ArrayList<String>> itemsfromhistoryarray;
    ArrayList<String> itemhldr;
    public String isEmpty = "true";
    public String uid,orderid,orderdate,address,contact,total,changefor,latitude,longitude,nongeoloc,status,ordername,landmark,additonalinst, deltype, delsched, coolerrental;
    public String driverlat, driverlong;
    public ArrayList<String> items;
    public String prefuid, prefemail, prefname, prefcontact, prefaddress, prefordertrans,preforderid;
    LocationManager mLocationManager;
    Location myLocation;
    String oncancelstatus = "false";

    //UI
    @BindView(R.id.orderlist)
    ListView orderlist;
    @BindView(R.id.header)
    TextView tvheader;

    FA_HistoryAdapter itemAdapter;

    MaterialDialog dialog;

    public R_MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clientorders, container, false);
        ButterKnife.bind(this, view);
        RetrieveUserData();
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
        tvheader.setTypeface(headertextfont);
    }

    public void InitializeFunctions(){
        if(prefordertrans.equals("true")){
            tvheader.setText("Existing Order"+"\n"
                             +"Order ID: " + preforderid + "\n"
                             +"Click here to return to Order ID");
            tvheader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment = new R_CurrentOrderFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.contentContainer, fragment);
                    fragmentTransaction.commit();
                }
            });
        }
            dialog = new MaterialDialog.Builder(getActivity())
                    .title("Loading Orders")
                    .content("Please wait...")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();

            lm = new C_OrderClass();
            listmenu = new ArrayList<>();
            orderdetails = new ArrayList<>();
            itemsfromhistoryarray = new ArrayList<>();
            itemhldr = new ArrayList<>();
            items = new ArrayList<>();
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();
            itemAdapter = new FA_HistoryAdapter(getActivity(), listmenu);
            orderlist.setAdapter(itemAdapter);
            RealtimeOrderHistory();

            orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                      AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                      LayoutInflater inflater = getActivity().getLayoutInflater();
                      View dialogView = inflater.inflate(R.layout.popup_orderclient, null);
                      dialogBuilder.setView(dialogView);
                      String valhldr = orderdetails.get(position);
                      itemhldr = itemsfromhistoryarray.get(position);
                      String stringtoparse = valhldr;
                      String parser = stringtoparse;
                      String delims = "[/]+";
                      String[] tokens = parser.split(delims);

                      final String ordid = String.valueOf(tokens[0]);
                      final String orddate = String.valueOf(tokens[1]);
                      final String ordadd = String.valueOf(tokens[2]);
                      final String ordcon = String.valueOf(tokens[3]);
                      final String ordtotal = String.valueOf(tokens[4]);
                      final String ordchangefor = String.valueOf(tokens[5]);
                      final String ordlat = String.valueOf(tokens[6]);
                      final String ordlong = String.valueOf(tokens[7]);
                      final String ordnongeo = String.valueOf(tokens[8]);
                      final String ordname = String.valueOf(tokens[9]);
                      final String ordlandmark = String.valueOf(tokens[10]);
                      final String ordadditional = String.valueOf(tokens[11]);
                      final String ordstatus = String.valueOf(tokens[12]);
                      final String userid = String.valueOf(tokens[13]);
                      final String orddeltype = String.valueOf(tokens[14]);
                      final String orddelsched = String.valueOf(tokens[15]);
                      final String orddelcooler = String.valueOf(tokens[16]);

                      final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
                      final TextView tvsubheader = (TextView) dialogView.findViewById(R.id.subheader);
                      final TextView tvordcontactheader = (TextView) dialogView.findViewById(R.id.contactheader);
                      final ListView listView = (ListView) dialogView.findViewById(R.id.checkoutcartlist);
                      final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                      final TextView tvordname = (TextView) dialogView.findViewById(R.id.delname);
                      final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                      final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                      final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                      final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                      final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                      final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);
                      final TextView tvlandmark = (TextView) dialogView.findViewById(R.id.dellandmark);
                      final TextView tvadditional = (TextView) dialogView.findViewById(R.id.deladditional);
                      final TextView tvscheduled = (TextView) dialogView.findViewById(R.id.delscheduled);
                      final TextView tvcooler = (TextView) dialogView.findViewById(R.id.delcooler);
                      final CircleImageView civcall = (CircleImageView) dialogView.findViewById(R.id.btn_call);
                      final CircleImageView civtext = (CircleImageView) dialogView.findViewById(R.id.btn_text);
                      final TextView tvcall = (TextView) dialogView.findViewById(R.id.tvcall);
                      final TextView tvtext = (TextView) dialogView.findViewById(R.id.tvtext);

                      final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
                      final Typeface subheadertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Black.otf");
                      final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");

                      tvheader.setTypeface(headertextfont);
                      tvsubheader.setTypeface(regulartextfont);
                      tvordcontactheader.setTypeface(regulartextfont);
                      tvordernum.setTypeface(regulartextfont);
                      tvordtotal.setTypeface(regulartextfont);
                      tvordstatus.setTypeface(regulartextfont);
                      tvordaddress.setTypeface(regulartextfont);
                      tvordcontact.setTypeface(regulartextfont);
                      tvordchange.setTypeface(regulartextfont);
                      tvordtimestamp.setTypeface(regulartextfont);
                      tvadditional.setTypeface(regulartextfont);
                      tvlandmark.setTypeface(regulartextfont);
                      tvscheduled.setTypeface(regulartextfont);
                      tvcooler.setTypeface(regulartextfont);
                      tvcall.setTypeface(regulartextfont);
                      tvtext.setTypeface(regulartextfont);
                      tvordname.setTypeface(regulartextfont);

                      final String deliveryschedhldr, coolerstatushldr;

                      if(orddeltype.equals("Now")){
                          deliveryschedhldr = "Scheduled Today";
                      }else{
                          deliveryschedhldr = orddelsched;
                      }

                      if(orddelcooler.equals("false")){
                          coolerstatushldr = "No";
                      }else{
                          coolerstatushldr = "Yes";
                      }

                      tvordernum.setText("Order ID: "+ordid);
                      tvordtotal.setText("Order Total: "+ordtotal);
                      tvordname.setText("Contact: " + ordname);
                      tvlandmark.setText("Nearest Landmark: " + ordlandmark);
                      tvadditional.setText("Additional Instructions: " + ordadditional);
                      tvordstatus.setText("Order Status: " + ordstatus);
                      tvordaddress.setText("Delivery Address: "+ordadd);
                      tvordcontact.setText("Contact Number: "+ordcon);
                      tvordchange.setText("Requested Change For: "+ordchangefor);
                      tvordtimestamp.setText("Delivery Timestamp: "+orddate);
                      tvcooler.setText("Cooler Rental: " + coolerstatushldr);
                      tvscheduled.setText("Order Schedule: " + deliveryschedhldr);

                      civcall.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordcon, null));
                              startActivity(intent);
                          }
                      });

                     tvcall.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View view) {
                             Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordcon, null));
                             startActivity(intent);
                         }
                     });

                      civtext.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ordcon, null)));
                          }
                      });

                      tvtext.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ordcon, null)));
                          }
                      });

                      setListViewHeightBasedOnChildren(listView);
                      listView.setAdapter(new FA_ListAdapter(getActivity(), ViewCartList()));
                                listView.setOnTouchListener(new View.OnTouchListener() {
                                    // Setting on Touch Listener for handling the touch inside ScrollView
                                    @Override
                                    public boolean onTouch(View v, MotionEvent event) {
                                        // Disallow the touch request for parent scroll on touch of child view
                                        v.getParent().requestDisallowInterceptTouchEvent(true);
                                        return false;
                                    }
                                });
                      final AlertDialog alertDialog = dialogBuilder.create();
                      Window window = alertDialog.getWindow();
                      window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                      window.setGravity(Gravity.CENTER);

                      FancyButton cancel_btn = (FancyButton) dialogView.findViewById(R.id.buttonclose);
                      FancyButton accept_btn = (FancyButton) dialogView.findViewById(R.id.buttonaccept);
                      TextView close_btn = (TextView) dialogView.findViewById(R.id.tvclose);

                      if(prefordertrans.equals("true")){
                          cancel_btn.setVisibility(View.INVISIBLE);
                          cancel_btn.setEnabled(false);
                          accept_btn.setVisibility(View.INVISIBLE);
                          accept_btn.setEnabled(false);
                      }else{
                          close_btn.setEnabled(true);
                          close_btn.setVisibility(View.VISIBLE);
                          close_btn.setOnClickListener(new View.OnClickListener() {
                              @Override
                              public void onClick(View view) {
                                  alertDialog.dismiss();
                              }
                          });

                          cancel_btn.setOnClickListener(new View.OnClickListener() {
                              @Override
                              public void onClick(View v) {
                                  if(ordstatus.equals("scheduled")){
                                      mDatabase.child("Orders").child(ordid).child("orderstatus").setValue("scheduledeclined");
                                      mDatabase.child("Users").child(userid).child("ScheduledOrders").child(ordid).child("orderstatus").setValue("scheduledeclined");
                                  }else{
                                      mDatabase.child("Orders").child(ordid).child("orderstatus").setValue("declined");
                                  }
                                  Toast.makeText(getActivity(), "You have declined this order", Toast.LENGTH_SHORT).show();
                                  alertDialog.dismiss();
                              }
                          });

                          accept_btn.setOnClickListener(new View.OnClickListener() {
                              @Override
                              public void onClick(View view) {
                                  AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                  LayoutInflater inflater = getActivity().getLayoutInflater();
                                  View dialogView = inflater.inflate(R.layout.popup_choice, null);
                                  dialogBuilder.setView(dialogView);

                                  final AlertDialog OptionalertDialog = dialogBuilder.create();
                                  Window window = OptionalertDialog.getWindow();
                                  window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                  window.setGravity(Gravity.CENTER);

                                  final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                                  final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                                  final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                                  optionheader.setTypeface(headertextfont);

                                  optionheader.setText("Accept Order?");

                                  negative.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          alertDialog.dismiss();
                                          OptionalertDialog.dismiss();
                                      }
                                  });

                                  positive.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          getLastKnownLocation();
                                          mDatabase.child("Orders").child(ordid).child("driverlat").setValue(driverlat);
                                          mDatabase.child("Orders").child(ordid).child("driverlong").setValue(driverlong);
                                          mDatabase.child("Orders").child(ordid).child("drivername").setValue(prefname);
                                          mDatabase.child("Orders").child(ordid).child("drivercontact").setValue(prefcontact);

                                          if(ordstatus.equals("scheduled")){
                                              mDatabase.child("Orders").child(ordid).child("orderstatus").setValue("scheduleaccepted");
                                              mDatabase.child("Users").child(userid).child("ScheduledOrders").child(ordid).child("orderstatus").setValue("scheduleaccepted");
                                          }else{
                                              mDatabase.child("Orders").child(ordid).child("orderstatus").setValue("otw");
                                          }

                                          SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                          editor.putString("ordertransaction", "true");
                                          editor.putString("orderuserid", userid);
                                          editor.putString("orderid", ordid);
                                          editor.putString("orderaddress", ordadd);
                                          editor.putString("ordercontact", ordcon);
                                          editor.putString("ordertimestamp", orddate);
                                          editor.putString("orderchangefor", ordchangefor);
                                          editor.putString("ordertotal", ordtotal);
                                          editor.putString("orderlatitude", ordlat);
                                          editor.putString("orderlongitude", ordlong);
                                          editor.putString("ordername", ordname);
                                          editor.putString("nongeolocadd", ordnongeo);
                                          editor.putString("orderlandmark", landmark);
                                          editor.putString("orderadditional", additonalinst);
                                          editor.putString("orderstatus", ordstatus);
                                          editor.putString("ordercooler", coolerstatushldr);
                                          editor.putString("orderdelsched", deliveryschedhldr);

                                          Set<String> set = new HashSet<String>();
                                          set.addAll(itemhldr);
                                          editor.putStringSet("orderitemarray", set);
                                          editor.apply();

                                          alertDialog.dismiss();
                                          OptionalertDialog.dismiss();

                                          Fragment fragment = new R_CurrentOrderFragment();
                                          FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                          FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                          fragmentTransaction.replace(R.id.contentContainer, fragment);
                                          fragmentTransaction.commit();
                                      }
                                  });
                                  OptionalertDialog.show();
                              }});
                         }
                      alertDialog.show();
            }});
            dialog.dismiss();
        }


    public void RealtimeOrderHistory(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Orders")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        uid = order.getUid();
                        orderid = order.getOrdernum();
                        orderdate = order.getTimestamp();
                        ordername = order.getContactname();
                        address = order.getAddress();
                        landmark = order.getLandmark();
                        additonalinst = order.getAdditional();
                        contact = order.getContactnum();
                        total = order.getCarttotal();
                        changefor = order.getChangefor();
                        status = order.getOrderstatus();
                        latitude = order.getLatitude();
                        longitude = order.getLongitude();
                        nongeoloc = order.getRealaddress();
                        items = order.getItems();
                        deltype = order.getDeliverytype();
                        delsched = order.getScheduledateandtime();
                        coolerrental = order.getCoolerrental();

                        if(uid !=null && orderid !=null && orderdate !=null && ordername !=null && address !=null && landmark !=null
                                && additonalinst !=null && contact !=null && total !=null && changefor !=null && status !=null && latitude !=null
                                && longitude !=null && nongeoloc !=null && items !=null && deltype !=null && delsched !=null && coolerrental !=null) {
                            if ((status.equals("pending")) || (status.equals("scheduled")) || (status.equals("trystatus"))) {
                                Context context = getApplicationContext();
                                Intent notificationIntent = new Intent(context, R_MainFragment.class);
                                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

                                if(getActivity() != null) {
                                    Notification myNotication;
                                    NotificationManager manager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
                                    Notification.Builder builder = new Notification.Builder(getActivity());
                                    builder.setAutoCancel(true);
                                    builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                                    builder.setLights(Color.YELLOW, 3000, 3000);
                                    builder.setTicker("New Order!!");
                                    builder.setContentTitle("Mags Delivery");
                                    builder.setContentText("New Order!");
                                    builder.setSmallIcon(R.drawable.magslogo);
                                    builder.setOngoing(false);
                                    builder.setContentIntent(intent);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        Bitmap Icon = BitmapFactory.decodeResource(getResources(), R.drawable.magslogo);
                                        builder.setLargeIcon(Icon);
                                    }
                                    builder.build();
                                    myNotication = builder.getNotification();
                                    manager.notify(11, myNotication);
                                }

                                lm = new C_OrderClass();
                                isEmpty = "false";
                                lm.setOrdernum(orderid);
                                lm.setTimestamp(orderdate);
                                lm.setAddress(address);
                                lm.setContactnum(contact);
                                lm.setCarttotal(total);
                                lm.setChangefor(changefor);
                                lm.setItems(items);
                                lm.setLatitude(latitude);
                                lm.setLongitude(longitude);
                                lm.setRealaddress(nongeoloc);
                                lm.setContactname(ordername);
                                lm.setLandmark(landmark);
                                lm.setAdditional(additonalinst);
                                lm.setOrderstatus(status);
                                lm.setUid(uid);
                                lm.setDeliverytype(deltype);
                                lm.setScheduledateandtime(delsched);
                                lm.setCoolerrental(coolerrental);
                                listmenu.add(lm);
                                orderdetails.add(orderid + "/" + orderdate + "/" + address + "/" + contact + "/" + total + "/" + changefor + "/" + latitude + "/" + longitude + "/" + nongeoloc + "/" + ordername + "/" + landmark + "/" + additonalinst + "/" + status + "/" + uid + "/" + deltype + "/" + delsched + "/" + coolerrental);
                                itemsfromhistoryarray.add(items);
                                itemAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        String changeorderid = order.getOrdernum();
                        String changeorderstatus = order.getOrderstatus();
                        if(changeorderid !=null && changeorderstatus !=null) {
                            if (changeorderstatus.equals("otw") || changeorderstatus.equals("delivered") || changeorderstatus.equals("declined") || changeorderstatus.equals("cancelled")) {
                                isEmpty = "false";
                                for (int val = 0; val < listmenu.size(); val++) {
                                    String changevalhldr = orderdetails.get(val);
                                    String changestringtoparse = changevalhldr;
                                    String changeparser = changestringtoparse;
                                    String changedelims = "[/]+";
                                    String[] changetokens = changeparser.split(changedelims);
                                    final String ordid = String.valueOf(changetokens[0]);
                                    if (ordid.equals(changeorderid)) {
                                        listmenu.remove(val);
                                        orderdetails.remove(val);
                                        itemsfromhistoryarray.remove(val);
                                        itemAdapter.notifyDataSetChanged();

                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

        private ArrayList ViewCartList(){
        ArrayList<C_MenuClass> listmenu = new ArrayList<>();
        C_MenuClass lm = new C_MenuClass();
        for(int i = 0; i<itemhldr.size();i++){
            lm = new C_MenuClass();
            String arrayvalhldr = itemhldr.get(i);
            String parser = arrayvalhldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    public void RetrieveUserData(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        prefordertrans = prefs.getString("ordertransaction", "order");
        prefuid = prefs.getString("userid", "UID");
        prefname = prefs.getString("username", "Name");
        prefemail = prefs.getString("useremail", "Email");
        prefaddress = prefs.getString("useraddress", "Address");
        prefcontact = prefs.getString("usercontact", "Contact");
        if(prefordertrans.equals("true")){
            preforderid = prefs.getString("orderid", "orderid");
        }
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else{
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                    driverlat = String.valueOf(bestLocation.getLatitude());
                    driverlong = String.valueOf(bestLocation.getLongitude());
                }
            }

        }
        return bestLocation;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AppBarLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
