package androidstudio.beerexpresss;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JP on 11/25/2017.
 */

public class FA_ListAdapter extends BaseAdapter {
    ArrayList<C_MenuClass> items;
    Context c;
    int pos;


    public FA_ListAdapter(Context c, ArrayList<C_MenuClass> items){
        this.c = c;
        this.items = items;
    }

    public void remove(int position){
        items.remove(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.row, viewGroup, false);
        }

        final C_MenuClass m = (C_MenuClass) this.getItem(i);
        TextView tvname = (TextView) view.findViewById(R.id.itemname);
        TextView tvprice = (TextView) view.findViewById(R.id.itemprice);
        TextView tvquantity = (TextView) view.findViewById(R.id.itemquantity);

        Typeface regulartextfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Montserrat-Light.otf");
        tvname.setTypeface(regulartextfont);
        tvprice.setTypeface(regulartextfont);
        tvquantity.setTypeface(regulartextfont);

        tvname.setText("Item Name: " + m.getItemname());
        tvprice.setText("Item Price: " + m.getItemprice());
        tvquantity.setText("Item Quantity: "+ m.getItemquan());
        return view;
    }
}
