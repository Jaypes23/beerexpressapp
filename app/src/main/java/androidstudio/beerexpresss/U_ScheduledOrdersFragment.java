package androidstudio.beerexpresss;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 11/27/2017.
 */

public class U_ScheduledOrdersFragment extends android.support.v4.app.Fragment {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    C_OrderClass lm;
    ArrayList<C_OrderClass> listmenu;
    ArrayList<String> orderdetails;
    ArrayList<ArrayList<String>> itemsfromhistoryarray;
    ArrayList<String> itemhldr;
    public String uid, orderid, orderdate, address, contact, total, changefor, dateandtime, additional, landmark;
    public ArrayList<String> items;
    public String status;

    //UI
    @BindView(R.id.historylist)
    ListView orderlist;
    @BindView(R.id.header)
    TextView header;

    FA_HistoryAdapter itemAdapter;

    MaterialDialog dialog;

    public U_ScheduledOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orderhistory, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
        header.setTypeface(headertextfont);
        header.setText("Scheduled Orders");
    }

    public void InitializeFunctions() {
        dialog = new MaterialDialog.Builder(getActivity())
                .title("Loading Scheduled Orders")
                .content("Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        lm = new C_OrderClass();
        listmenu = new ArrayList<>();
        orderdetails = new ArrayList<>();
        itemsfromhistoryarray = new ArrayList<>();
        itemhldr = new ArrayList<>();
        items = new ArrayList<>();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        itemAdapter = new FA_HistoryAdapter(getActivity(), listmenu);
        orderlist.setAdapter(itemAdapter);

        RetrieveOrderHistory();

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_scheduledorderinfo, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                String valhldr = orderdetails.get(position);
                itemhldr = itemsfromhistoryarray.get(position);
                String stringtoparse = valhldr;
                String parser = stringtoparse;
                String delims = "[/]+";
                String[] tokens = parser.split(delims);
                String ordid = String.valueOf(tokens[0]);
                String orddate = String.valueOf(tokens[1]);
                String ordadd = String.valueOf(tokens[2]);
                String ordcon = String.valueOf(tokens[3]);
                String ordtotal = String.valueOf(tokens[4]);
                String ordchangefor = String.valueOf(tokens[5]);
                final String ordstatus = String.valueOf(tokens[6]);
                String orddateandtime = String.valueOf(tokens[7]);
                String ordadditional = String.valueOf(tokens[8]);
                String ordlandmark = String.valueOf(tokens[9]);

                final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
                final ListView listView = (ListView) dialogView.findViewById(R.id.deliveryitemlist);
                final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);
                final TextView tvordadditional = (TextView) dialogView.findViewById(R.id.deladditional);
                final TextView tvlandmark = (TextView) dialogView.findViewById(R.id.dellandmark);
                final TextView tvrider = (TextView) dialogView.findViewById(R.id.delridername);
                final TextView tvdateandtime = (TextView) dialogView.findViewById(R.id.delschedule);
                final CircleImageView cvcall = (CircleImageView) dialogView.findViewById(R.id.btn_call);
                final TextView tvcall = (TextView) dialogView.findViewById(R.id.tvcall);
                final CircleImageView cvtext = (CircleImageView) dialogView.findViewById(R.id.btn_text);
                final TextView tvtext = (TextView) dialogView.findViewById(R.id.tvtext);
                final TextView tvordcontactheader = (TextView) dialogView.findViewById(R.id.contactheader);
                final FancyButton btnclose = (FancyButton) dialogView.findViewById(R.id.buttonclose);
                final FancyButton btncancel = (FancyButton) dialogView.findViewById(R.id.btncancelorder);


                final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
                final Typeface subheadertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Black.otf");
                final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");

                tvheader.setTypeface(headertextfont);
                tvordcontactheader.setTypeface(regulartextfont);
                tvordernum.setTypeface(regulartextfont);
                tvordtotal.setTypeface(regulartextfont);
                tvordstatus.setTypeface(regulartextfont);
                tvordaddress.setTypeface(regulartextfont);
                tvordcontact.setTypeface(regulartextfont);
                tvordchange.setTypeface(regulartextfont);
                tvordtimestamp.setTypeface(regulartextfont);
                tvordadditional.setTypeface(regulartextfont);
                tvlandmark.setTypeface(regulartextfont);
                tvdateandtime.setTypeface(regulartextfont);
                tvrider.setTypeface(regulartextfont);
                tvcall.setTypeface(regulartextfont);
                tvtext.setTypeface(regulartextfont);
                tvdateandtime.setTypeface(regulartextfont);

                cvcall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
                        startActivity(intent);
                    }
                });

                cvtext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", "09176727581", null)));
                    }
                });

                tvcall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
                        startActivity(intent);
                    }
                });

                tvtext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
                        startActivity(intent);
                    }
                });

                btnclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                tvordernum.setText("Order ID: " + ordid);
                tvordtotal.setText("Order Total: " + ordtotal);
                tvordstatus.setText("Delivered");
                tvordaddress.setText("Your Delivery Address: " + ordadd);
                tvordcontact.setText("Your Contact Number: " + ordcon);
                tvordchange.setText("Requested Change For: " + ordchangefor);
                tvordtimestamp.setText("Delivery Timestamp: " + orddate);
                tvordstatus.setText("Scheduled Delivery");
                tvordadditional.setText("Additional Instructions: " + ordadditional);
                tvlandmark.setText("Landmark: " + ordlandmark);
                tvdateandtime.setText("Scheduled Date & Time: " + orddateandtime);

                setListViewHeightBasedOnChildren(listView);
                listView.setAdapter(new FA_ListAdapter(getActivity(), ViewCartList()));
                listView.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });

                // Cancel Button
                btnclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                btncancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(ordstatus.equals("scheduleaccepted")){
                            Toast.makeText(getActivity(), "You have can't cancel anymore. Order has been accepted", Toast.LENGTH_SHORT).show();
                        }else{
                            orderlist.setAdapter(null);
                            Toast.makeText(getActivity(), "You have cancelled your order", Toast.LENGTH_SHORT).show();
                            mDatabase.child("Orders").child(orderid).child("orderstatus").setValue("schedulecancelled");
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("ScheduledOrders").child(orderid).child("orderstatus").setValue("schedulecancelled");
                            orderlist.setAdapter(null);
                            itemAdapter.notifyDataSetChanged();
                            listmenu = new ArrayList<>();
                            orderdetails = new ArrayList<>();
                            itemsfromhistoryarray = new ArrayList<>();
                            itemhldr = new ArrayList<>();
                            items = new ArrayList<>();
                            mAuth = FirebaseAuth.getInstance();
                            mDatabase = FirebaseDatabase.getInstance().getReference();
                            itemAdapter = new FA_HistoryAdapter(getActivity(), listmenu);
                            orderlist.setAdapter(itemAdapter);
                            RetrieveOrderHistory();
                            itemAdapter.notifyDataSetChanged();
                        }
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();
            }
        });
        dialog.dismiss();
    }


    public void RetrieveOrderHistory(){
        FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Orders")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        uid = order.getUid();
                        orderid = order.getOrdernum();
                        orderdate = order.getTimestamp();
                        address = order.getAddress();
                        contact = order.getContactnum();
                        total = order.getCarttotal();
                        changefor = order.getChangefor();
                        status = order.getOrderstatus();
                        items = order.getItems();
                        dateandtime = order.getScheduledateandtime();
                        additional = order.getAdditional();
                        landmark = order.getLandmark();

                        if(uid !=null && orderid !=null && orderdate !=null && address !=null && contact !=null && total !=null && changefor !=null && items !=null && status !=null && dateandtime !=null && additional !=null && landmark !=null) {
                            if (mAuth.getCurrentUser().getUid().equals(uid)) {
                                if ((status.equals("scheduled")) || (status.equals("scheduleaccepted")) || (status.equals("scheduledeclined")) || (status.equals("scheduledelivered")) || (status.equals("schedulecancelled"))) {
                                    if (status.equals("scheduled")) {
                                        status = "Scheduled Order";
                                    }
                                    if (status.equals("scheduleaccepted")) {
                                        status = "Scheduled Order Accepted";
                                    }
                                    if (status.equals("scheduledeclined")) {
                                        status = "Scheduled Order Declined";
                                    }
                                    if (status.equals("scheduledelivered")) {
                                        status = "Scheduled Order Delivered";
                                    }
                                    if (status.equals("schedulecancelled")) {
                                        status = "Scheduled Order Cancelled";
                                    }

                                    lm = new C_OrderClass();
                                    lm.setOrdernum(orderid);
                                    lm.setTimestamp(orderdate);
                                    lm.setAddress(address);
                                    lm.setContactnum(contact);
                                    lm.setCarttotal(total);
                                    lm.setChangefor(changefor);
                                    lm.setItems(items);
                                    lm.setOrderstatus(status);
                                    lm.setScheduledateandtime(dateandtime);
                                    lm.setAdditional(additional);
                                    lm.setLandmark(landmark);

                                    listmenu.add(lm);
                                    orderdetails.add(orderid + "/" + orderdate + "/" + address + "/" + contact + "/" + total + "/" + changefor + "/" + status + "/" + dateandtime + "/" + additional + "/" + landmark);
                                    itemsfromhistoryarray.add(items);
                                    itemAdapter.notifyDataSetChanged();
                                }
                                dialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                        uid = order.getUid();
                        orderid = order.getOrdernum();
                        orderdate = order.getTimestamp();
                        address = order.getAddress();
                        contact = order.getContactnum();
                        total = order.getCarttotal();
                        changefor = order.getChangefor();
                        status = order.getOrderstatus();
                        items = order.getItems();
                        dateandtime = order.getScheduledateandtime();
                        additional = order.getAdditional();
                        landmark = order.getLandmark();

                        if(mAuth.getCurrentUser().getUid().equals(uid)) {
                            if ((status.equals("scheduled")) || (status.equals("scheduleaccepted")) || (status.equals("scheduledeclined")) || (status.equals("scheduledelivered")) || (status.equals("schedulecancelled")) || (status.equals("delivered")) || (status.equals("accepted"))) {
                                orderlist.setAdapter(null);
                                itemAdapter.notifyDataSetChanged();
                                listmenu = new ArrayList<>();
                                orderdetails = new ArrayList<>();
                                itemsfromhistoryarray = new ArrayList<>();
                                itemhldr = new ArrayList<>();
                                items = new ArrayList<>();
                                mAuth = FirebaseAuth.getInstance();
                                mDatabase = FirebaseDatabase.getInstance().getReference();
                                itemAdapter = new FA_HistoryAdapter(getActivity(), listmenu);
                                orderlist.setAdapter(itemAdapter);
                                RetrieveOrderHistory();
                                itemAdapter.notifyDataSetChanged();
                            }
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {}

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }


    private ArrayList ViewCartList(){
        ArrayList<C_MenuClass> listmenu = new ArrayList<>();
        C_MenuClass lm = new C_MenuClass();
        for(int i = 0; i<itemhldr.size();i++){
            lm = new C_MenuClass();
            String arrayvalhldr = itemhldr.get(i);
            String parser = arrayvalhldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AppBarLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
