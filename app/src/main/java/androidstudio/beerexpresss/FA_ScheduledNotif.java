package androidstudio.beerexpresss;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by JP on 3/4/2018.
 */

public class FA_ScheduledNotif extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, U_MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);

        Notification myNotication;
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);
        builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        builder.setLights(Color.YELLOW, 3000, 3000);
        builder.setTicker("You have a Scheduled Delivery today!");
        builder.setContentTitle("Mags Delivery");
        builder.setContentText("You have a Scheduled Delivery today!");
        builder.setSmallIcon(R.drawable.magslogo);
        builder.setOngoing(false);
        builder.setContentIntent(contentIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bitmap Icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.magslogo);
            builder.setLargeIcon(Icon);
        }
        builder.build();
        myNotication = builder.getNotification();
        manager.notify(11, myNotication);

    }
}
