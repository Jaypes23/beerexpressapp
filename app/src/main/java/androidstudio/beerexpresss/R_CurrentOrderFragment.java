package androidstudio.beerexpresss;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/1/2017.
 */

public class R_CurrentOrderFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, orderaddress, ordercontact, ordertimestamp, orderchangefor, ordertotal, orderlat, orderlong, ordernongeoadd, ordername, orderlandmark, orderadditional, orderstatus, orderuserid, ordercooler, orderdelsched;
    ArrayList<String> orderitemsarray;
    String arrayitemshldr;
    public String getLocStat = "true";

    LocationManager mLocationManager;
    String latitude, longitude;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    MaterialDialog dialog;

    @BindView(R.id.header)
    TextView tvheader;
    @BindView(R.id.subheader)
    TextView tvsubheader;
    @BindView(R.id.contactheader)
    TextView tvcontactheader;
    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.delname)
    TextView tvordername;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView(R.id.delcontact)
    TextView tvordercontact;
    @BindView(R.id.delchangefor)
    TextView tvorderchangefor;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.deladditional)
    TextView tvadditional;
    @BindView(R.id.dellandmark)
    TextView tvlandmark;
    @BindView(R.id.delscheduled)
    TextView tvschedule;
    @BindView(R.id.delcooler)
    TextView tvcooler;
    @BindView(R.id.deliveryitemlist)
    ListView lvitemlist;
    @BindView(R.id.buttongetdirections)
    FancyButton btnGetDirections;
    @BindView(R.id.buttonfinishorder)
    FancyButton btnFinish;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.tvback)
    TextView tvback;
    @BindView(R.id.btn_text)
    CircleImageView btnText;
    @BindView(R.id.tvcall)
    TextView tvcall;
    @BindView(R.id.tvtext)
    TextView tvtext;


    public R_CurrentOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_currentorder, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
        final Typeface subheadertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Black.otf");
        final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");

        tvheader.setTypeface(headertextfont);
        tvsubheader.setTypeface(subheadertextfont);
        tvcontactheader.setTypeface(subheadertextfont);
        tvorderid.setTypeface(regulartextfont);
        tvadditional.setTypeface(regulartextfont);
        tvlandmark.setTypeface(regulartextfont);
        tvordername.setTypeface(regulartextfont);
        tvordercontact.setTypeface(regulartextfont);
        tvordertimestamp.setTypeface(regulartextfont);
        tvordertotal.setTypeface(regulartextfont);
        tvorderchangefor.setTypeface(regulartextfont);
        tvorderstatus.setTypeface(regulartextfont);
        tvcooler.setTypeface(regulartextfont);
        tvschedule.setTypeface(regulartextfont);
        tvtext.setTypeface(regulartextfont);
        tvcall.setTypeface(regulartextfont);
        tvorderaddress.setTypeface(regulartextfont);
    }

    public void InitializeFunctions() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        RetrieveOrderData();
        getLastKnownLocation();
        onChangeLocation();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordercontact, null));
                startActivity(intent);
            }
        });

        tvcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordercontact, null));
                startActivity(intent);
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ordercontact, null)));
            }
        });

        tvtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.FragmentTransaction transaction;
                Fragment fragmentsreport = new R_MainFragment();
                transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.replace(R.id.contentContainer, fragmentsreport);
                transaction.commit();
            }
        });

        btnGetDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String uri = "https://waze.com:/ul?ll="+orderlat+","+orderlong+"&navigate=yes";
//                startActivity(new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse(uri)));
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+orderlat+","+orderlong));
                startActivity(intent);
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_choice, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog OptionalertDialog = dialogBuilder.create();
                Window window = OptionalertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");

                optionheader.setTypeface(headertextfont);
                optionheader.setText("Finish Order?");

                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OptionalertDialog.dismiss();
                    }
                });

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getLocStat = "false";
                        dialog = new MaterialDialog.Builder(getActivity())
                                .title("Delivery Successful!")
                                .content("You can now choose a new customer")
                                .positiveText("Close")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                        if(orderstatus.equals("scheduled")){
                                            mDatabase.child("Users").child(orderuserid).child("ScheduledOrders").child(orderid).child("orderstatus").setValue("scheduledelivered");
                                        }
                                        mDatabase.child("Orders").child(orderid).child("orderstatus").setValue("delivered");
                                        mDatabase.child("Users").child(orderuserid).child("currentorderid").setValue("none");

                                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString("ordertransaction", "false");
                                        editor.putString("orderid", "none");
                                        editor.putString("orderaddress", "none");
                                        editor.putString("ordercontact", "none");
                                        editor.putString("ordername", "none");
                                        editor.putString("orderlandmark", "none");
                                        editor.putString("orderadditional", "none");
                                        editor.putString("ordertimestamp", "none");
                                        editor.putString("orderchangefor", "none");
                                        editor.putString("ordertotal", "none");
                                        editor.putString("orderlatitude", "none");
                                        editor.putString("orderlongitude", "none");
                                        editor.putString("nongeolocadd", "none");
                                        editor.putString("orderstatus", "none");
                                        editor.putString("orderuserid", "none");
                                        editor.putString("ordercooler", "none");
                                        editor.putString("orderdelsched", "none");

                                        orderitemsarray = new ArrayList<>();
                                        Set<String> set = new HashSet<String>();
                                        set.addAll(orderitemsarray);
                                        editor.putStringSet("orderitemarray", set);
                                        editor.apply();

                                        OptionalertDialog.dismiss();

                                        Fragment fragment = new R_MainFragment();
                                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.contentContainer, fragment);
                                        fragmentTransaction.remove(new R_CurrentOrderFragment());
                                        fragmentTransaction.commit();
                                    }
                                })
                                .show();
                    }
                });
                OptionalertDialog.show();
            }
        });
    }



    public void RetrieveOrderData() {
        orderitemsarray = new ArrayList<>();
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("orderid", "ID");
        orderaddress = prefs.getString("orderaddress", "Address");
        ordercontact = prefs.getString("ordercontact", "Contact");
        ordertimestamp = prefs.getString("ordertimestamp", "Timestamp");
        orderchangefor = prefs.getString("orderchangefor", "ChangeFor");
        ordertotal = prefs.getString("ordertotal", "Total");
        orderlat = prefs.getString("orderlatitude", "Latitude");
        orderlong = prefs.getString("orderlongitude", "Longitude");
        ordernongeoadd = prefs.getString("nongeolocadd", "NonGeoAdd");
        ordername = prefs.getString("ordername", "Contact Name");
        orderlandmark = prefs.getString("orderlandmark", "Landmark");
        orderadditional = prefs.getString("orderadditional", "Additional");
        orderstatus = prefs.getString("orderstatus", "Status");
        orderuserid = prefs.getString("orderuserid", "UID");
        orderdelsched = prefs.getString("orderdelsched", "Sched");
        ordercooler = prefs.getString("ordercooler", "Cooler");

        Set<String> set = prefs.getStringSet("orderitemarray", null);
        orderitemsarray = new ArrayList<String>(set);

        if (orderaddress.equals(ordernongeoadd)) {
            tvorderaddress.setText("Customer Delivery Address: " + orderaddress);
        } else {
            tvorderaddress.setText("Customer Delivery Address: " + orderaddress
                    + "\n" + "Customer Non Google Maps Address: " + ordernongeoadd);
        }

        tvorderid.setText("Current Order ID: " + orderid);
        tvadditional.setText("Additional Instructions: " + orderadditional);
        tvlandmark.setText("Nearest Landmark: " + orderlandmark);
        tvordername.setText("Contact Name: " + ordername);
        tvordercontact.setText("Customer Contact Number: " + ordercontact);
        tvordertimestamp.setText("Delivery Timestamp: " + ordertimestamp);
        tvordertotal.setText("Order Total: " + ordertotal);
        tvorderchangefor.setText("Requested Change For: " + orderchangefor);
        tvorderstatus.setText("Delivery not yet finished!");
        tvcooler.setText("Cooler Rental: " + ordercooler);
        tvschedule.setText("Order Schedule: " + orderdelsched);

        setListViewHeightBasedOnChildren(lvitemlist);
        lvitemlist.setAdapter(new FA_ListAdapter(getActivity(), ViewCartList()));
        lvitemlist.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    private ArrayList ViewCartList(){
        ArrayList<C_MenuClass> listmenu = new ArrayList<>();
        C_MenuClass lm = new C_MenuClass();
        for(int i = 0; i<orderitemsarray.size();i++){
            lm = new C_MenuClass();
            arrayitemshldr = orderitemsarray.get(i);
            String parser = arrayitemshldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    private Location getLastKnownLocation() {
        Location bestLocation = null;
        if(getActivity() != null){
            mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);
            for (String provider : providers) {
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission Check");
                    Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    return null;
                } else {
                    Location l = mLocationManager.getLastKnownLocation(provider);
                    if (l == null) {
                        continue;
                    }
                    if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                        bestLocation = l;
                        latitude = String.valueOf(bestLocation.getLatitude());
                        longitude = String.valueOf(bestLocation.getLongitude());

                        mDatabase.child("Orders").child(orderid).child("driverlat").setValue(latitude);
                        mDatabase.child("Orders").child(orderid).child("driverlong").setValue(longitude);
                    }
                }
            }
        }
        return bestLocation;
    }


    public void onChangeLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        {
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    getLastKnownLocation();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {}

                @Override
                public void onProviderEnabled(String s) {}

                @Override
                public void onProviderDisabled(String s) {}
            };
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    locationListener);
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AppBarLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
