package androidstudio.beerexpresss;

/**
 * Created by JP on 11/23/2017.
 */

public interface FA_CartListener {
    //Cart Manipulation
    void AddtoCart(String itemcart, String total);
    void UpdateCart(int index, String itemcart, String total);
    void RemovefromCart(int index);
    void TriggerCartManagement(String validation);
}
