package androidstudio.beerexpresss;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by JP on 12/2/2017.
 */

public class MyApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
