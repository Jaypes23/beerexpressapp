package androidstudio.beerexpresss;

/**
 * Created by JP on 11/23/2017.
 */

public class C_UserClass {
    public String name;
    public String address;
    public String contact;
    public String birthday;
    public String age;
    public String email;
    public String password;
    public String userstatus;
    public String currentorderid;

    public C_UserClass(){

    }

    public C_UserClass(String name, String address, String contact, String birthday, String age, String email, String userstatus, String currentorderid) {
        this.name = name;
        this.address = address;
        this.contact = contact;
        this.birthday = birthday;
        this.age = age;
        this.email = email;
        this.userstatus = userstatus;
        this.currentorderid = currentorderid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    public String getCurrentorderid() {
        return currentorderid;
    }

    public void setCurrentorderid(String currentorderid) {
        this.currentorderid = currentorderid;
    }
}
