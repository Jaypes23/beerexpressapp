package androidstudio.beerexpresss;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 11/27/2017.
 */

public class U_CurrentOrderFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, orderaddress, ordercontact, ordertimestamp, orderchangefor, ordertotal, orderlat, orderlong, ordernongeoadd, orderstat;
    String orderlandmark,orderadditional;
    String firebaseorderstatus;
    String drivercontactcheck,drivernamecheck;
    String drivername;
    String driverready = "false";
    ArrayList<String> orderitemsarray;
    String arrayitemshldr;
    Activity activity;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    MaterialDialog contactdialog,delivereddialog,otwdialog,declineddialog;

    @BindView(R.id.header)
    TextView tvheader;
    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView(R.id.delcontact)
    TextView tvordercontact;
    @BindView(R.id.delchangefor)
    TextView tvorderchangefor;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.delridername)
    TextView tvridername;
    @BindView(R.id.dellandmark)
    TextView tvorderlandmark;
    @BindView(R.id.deladditional)
    TextView tvadditional;
    @BindView(R.id.itemsubheader)
    TextView tvsubheader;
    @BindView(R.id.deliveryitemlist)
    ListView lvitemlist;
    @BindView(R.id.buttoncancel)
    FancyButton btnCancel;
//    @BindView(R.id.buttontrackorder)
//    FancyButton btnTrack;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;
    @BindView(R.id.tvcall)
    TextView tvcall;
    @BindView(R.id.tvtext)
    TextView tvtext;
    @BindView(R.id.contactheader)
    TextView tvcontactheader;

    public U_CurrentOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        final Typeface headertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Bold.otf");
        final Typeface subheadertextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Black.otf");
        final Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Montserrat-Medium.otf");

        tvheader.setTypeface(headertextfont);
        tvorderstatus.setTypeface(subheadertextfont);
        tvordertotal.setTypeface(subheadertextfont);
        tvcontactheader.setTypeface(regulartextfont);
        tvorderaddress.setTypeface(regulartextfont);
        tvordercontact.setTypeface(regulartextfont);
        tvorderlandmark.setTypeface(regulartextfont);
        tvorderchangefor.setTypeface(regulartextfont);
        tvordertimestamp.setTypeface(regulartextfont);
        tvadditional.setTypeface(regulartextfont);
        tvsubheader.setTypeface(regulartextfont);
        tvorderid.setTypeface(regulartextfont);
        tvridername.setTypeface(regulartextfont);
        tvtext.setTypeface(regulartextfont);
        tvcall.setTypeface(regulartextfont);
    }

    public void InitializeFunctions(){
        activity = getActivity();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
        new RetrieveOrderStatus().execute();
        RetrieveOrderData();

        contactdialog = new MaterialDialog.Builder(getActivity())
                .title("Contact Rider")
                .content("Please Text or Call/Miss Call immediately the rider to notify him. Use the buttons that would be provided in the screen below")
                .positiveText("Okay")
                .cancelable(false)
                .show();

        //        btnTrack.setEnabled(true);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
                startActivity(intent);
//                if(driverready.equals("false")){
//                    Toast.makeText(activity, "There is no rider yet. Please wait", Toast.LENGTH_SHORT).show();
//                }else{
//                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
//                    startActivity(intent);
//                }
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", "09176727581", null)));
//                if(driverready.equals("false")){
//                    Toast.makeText(activity, "There is no rider yet. Please wait", Toast.LENGTH_SHORT).show();
//                }else{
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", "09176727581", null)));
//                }
            }
        });

        tvcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
                startActivity(intent);
//                if(driverready.equals("false")){
//                    Toast.makeText(activity, "There is no rider yet. Please wait", Toast.LENGTH_SHORT).show();
//                }else{
//                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
//                    startActivity(intent);
//                }
            }
        });

        tvtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
                startActivity(intent);
//                if(driverready.equals("false")){
//                    Toast.makeText(activity, "There is no rider yet. Please wait", Toast.LENGTH_SHORT).show();
//                }else{
//                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "09176727581", null));
//                    startActivity(intent);
//                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(driverready.equals("false")){
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.popup_choice, null);
                    dialogBuilder.setView(dialogView);

                    final AlertDialog OptionalertDialog = dialogBuilder.create();
                    Window window = OptionalertDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER);

                    final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                    final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                    final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                    optionheader.setText("Cancel Order?");

                    negative.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OptionalertDialog.dismiss();
                        }
                    });

                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getActivity(), "You have cancelled your order", Toast.LENGTH_SHORT).show();
                            editor.putString("ordertransaction", "false");
                            editor.putString("orderid", "none");
                            editor.putString("orderaddress", "none");
                            editor.putString("ordercontact", "none");
                            editor.putString("ordertimestamp", "none");
                            editor.putString("orderlandmark", "none");
                            editor.putString("orderadditional", "none");
                            editor.putString("orderchangefor", "none");
                            editor.putString("ordertotal", "none");
                            editor.putString("orderlatitude", "none");
                            editor.putString("orderlongitude", "none");
                            editor.putString("nongeolocadd", "none");

                            orderitemsarray = new ArrayList<>();
                            Set<String> set = new HashSet<String>();
                            set.addAll(orderitemsarray);
                            editor.putStringSet("orderitemarray", set);
                            editor.apply();

                            mDatabase.child("Orders").child(orderid).child("orderstatus").setValue("cancelled");
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorderid").setValue("none");

                            OptionalertDialog.dismiss();

                            Fragment fragment = new U_MainFragment();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.remove(new U_CurrentOrderFragment());
                            fragmentTransaction.replace(R.id.frame_container, fragment);
                            fragmentTransaction.commit();
                        }
                    });
                    OptionalertDialog.show();
                }else{
                    Toast.makeText(activity, "Order was approved. Cannot cancel anymore", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        btnTrack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(driverready.equals("false")){
//                    Toast.makeText(activity, "There is no rider yet. Please wait", Toast.LENGTH_SHORT).show();
//                }else{
//                    Intent intent = new Intent(getActivity(), U_TrackRiderActivity.class);
//                    startActivity(intent);
//                }
//            }
//        });
    }

    public void RetrieveOrderData(){
        orderitemsarray = new ArrayList<>();
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("orderid", "ID");
        orderaddress = prefs.getString("orderaddress", "Address");
        ordercontact = prefs.getString("ordercontact", "Contact");
        ordertimestamp = prefs.getString("ordertimestamp", "Timestamp");
        orderchangefor = prefs.getString("orderchangefor", "ChangeFor");
        orderlandmark = prefs.getString("orderlandmark", "Landmark");
        orderadditional = prefs.getString("orderadditional", "OrderAdditional");
        ordertotal = prefs.getString("ordertotal", "Total");
        orderlat = prefs.getString("orderlatitude", "Latitude");
        orderlong = prefs.getString("orderlongitude", "Longitude");
        orderstat = prefs.getString("orderstatus", "Status");
        ordernongeoadd = prefs.getString("nongeolocadd", "NonGeoAdd");
        Set<String> set = prefs.getStringSet("orderitemarray", null);
        orderitemsarray = new ArrayList<String>(set);

        tvorderid.setText("Order ID: "+orderid);
        tvorderlandmark.setText("Your Nearest Landmark: "+orderlandmark);
        tvadditional.setText("Additional Instructions: "+orderadditional);
        tvorderaddress.setText("Your Delivery Address: "+orderaddress);
        tvordercontact.setText("Your Contact Number: "+ordercontact);
        tvordertimestamp.setText("Delivery Timestamp: "+ordertimestamp);
        tvordertotal.setText("Order Total: "+ordertotal);
        tvorderchangefor.setText("Requested Change For: "+orderchangefor);
        tvridername.setText("Rider Name: No Rider Yet ");
        tvorderstatus.setText("Wait for Rider Approval");

        setListViewHeightBasedOnChildren(lvitemlist);
        lvitemlist.setAdapter(new FA_ListAdapter(getActivity(), ViewCartList()));
        lvitemlist.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lvitemlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int itemPosition = i;
                String  itemValue = (String) lvitemlist.getItemAtPosition(i);
                Toast.makeText(getActivity(),
                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private ArrayList ViewCartList(){
        ArrayList<C_MenuClass> listmenu = new ArrayList<>();
        C_MenuClass lm = new C_MenuClass();
        for(int i = 0; i<orderitemsarray.size();i++){
            lm = new C_MenuClass();
            arrayitemshldr = orderitemsarray.get(i);
            String parser = arrayitemshldr;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String itemname = String.valueOf(tokens[0]);
            String itemprice = String.valueOf(tokens[1]);
            String itemquan = String.valueOf(tokens[2]);
            lm.setItemname(itemname);
            lm.setItemprice(itemprice);
            lm.setItemquan(Integer.parseInt(itemquan));
            listmenu.add(lm);
        }
        return  listmenu;
    }

    class RetrieveOrderStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FA_FirebaseClass firebaseFunctions = new FA_FirebaseClass(getActivity());
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
                mRootRef.child("Orders")
                        .child(orderid)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                final C_OrderClass order = dataSnapshot.getValue(C_OrderClass.class);
                                firebaseorderstatus = order.getOrderstatus();
                                drivercontactcheck = order.getDrivercontact();
                                drivernamecheck = order.getDrivername();

                                if (firebaseorderstatus != null && drivercontactcheck != null && drivernamecheck != null) {
                                    if (firebaseorderstatus.equals("delivered")) {
                                        if(delivereddialog == null && activity != null) {
                                            delivereddialog = new MaterialDialog.Builder(activity)
                                                    .title("Delivery Successful!")
                                                    .content("Thank you for using Mags Delivery!")
                                                    .positiveText("Close")
                                                    .cancelable(false)
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            editor.putString("ordertransaction", "false");
                                                            editor.putString("orderid", "none");
                                                            editor.putString("orderaddress", "none");
                                                            editor.putString("ordercontact", "none");
                                                            editor.putString("ordertimestamp", "none");
                                                            editor.putString("orderlandmark", "none");
                                                            editor.putString("orderadditional", "none");
                                                            editor.putString("orderchangefor", "none");
                                                            editor.putString("ordertotal", "none");
                                                            editor.putString("orderlatitude", "none");
                                                            editor.putString("orderlongitude", "none");
                                                            editor.putString("nongeolocadd", "none");

                                                            orderitemsarray = new ArrayList<>();
                                                            Set<String> set = new HashSet<String>();
                                                            set.addAll(orderitemsarray);
                                                            editor.putStringSet("orderitemarray", set);
                                                            editor.apply();

                                                            dialog.dismiss();

                                                            Fragment fragment = new U_MainFragment();
                                                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                            fragmentTransaction.remove(new U_CurrentOrderFragment());
                                                            fragmentTransaction.replace(R.id.frame_container, fragment);
                                                            fragmentTransaction.commit();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                    if (firebaseorderstatus.equals("otw")) {
                                        if (otwdialog == null && activity != null) {
                                            driverready = "true";
                                            otwdialog = new MaterialDialog.Builder(activity)
                                                    .title("Delivery Update!")
                                                    .content("Rider is on the way!")
                                                    .positiveText("Close")
                                                    .cancelable(false)
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            tvorderstatus.setText("Order accepted. 30 min to 1 hour delivery time");
//                                            if(!drivercontactcheck.equals("ND")){
//                                                drivercontactnum = drivercontactcheck;
//                                                driverready = "true";
//                                            }
                                                            if (!drivernamecheck.equals("ND")) {
                                                                drivername = drivernamecheck;
                                                                tvridername.setText("Rider Name: " + drivername);
                                                            }
                                                            dialog.dismiss();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                    if (firebaseorderstatus.equals("declined")) {
                                        if (declineddialog == null && activity != null) {
                                            declineddialog = new MaterialDialog.Builder(activity)
                                                    .title("Delivery Update!")
                                                    .content("Order Declined. Mags Delivery cannot serve you now. We're sorry!")
                                                    .positiveText("Close")
                                                    .cancelable(false)
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            editor.putString("ordertransaction", "false");
                                                            editor.putString("orderid", "none");
                                                            editor.putString("orderaddress", "none");
                                                            editor.putString("ordercontact", "none");
                                                            editor.putString("ordertimestamp", "none");
                                                            editor.putString("orderlandmark", "none");
                                                            editor.putString("orderadditional", "none");
                                                            editor.putString("orderchangefor", "none");
                                                            editor.putString("ordertotal", "none");
                                                            editor.putString("orderlatitude", "none");
                                                            editor.putString("orderlongitude", "none");
                                                            editor.putString("nongeolocadd", "none");

                                                            orderitemsarray = new ArrayList<>();
                                                            Set<String> set = new HashSet<String>();
                                                            set.addAll(orderitemsarray);
                                                            editor.putStringSet("orderitemarray", set);
                                                            editor.apply();

                                                            dialog.dismiss();

                                                            Fragment fragment = new U_MainFragment();
                                                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                            fragmentTransaction.remove(new U_CurrentOrderFragment());
                                                            fragmentTransaction.replace(R.id.frame_container, fragment);
                                                            fragmentTransaction.commit();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
            return null;
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AppBarLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
